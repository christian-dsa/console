
[![pipeline status](https://gitlab.com/christian-dsa/console/badges/develop/pipeline.svg)](https://gitlab.com/christian-dsa/console/commits/develop)
[![coverage report](https://gitlab.com/christian-dsa/console/badges/develop/coverage.svg)](https://gitlab.com/christian-dsa/console/-/commits/develop)
[![Latest Release](https://gitlab.com/christian-dsa/console/-/badges/release.svg)](https://gitlab.com/christian-dsa/console/-/releases)

# Console
Console written in c++ for embedded systems using an RTOS or bare metal. It is designed to be easy to integrate in 
a project, simple to use and lightweight.

## Requirement
* c++ 17 compiler

## Memory footprint
Compiling the console for an ARM cortex-m4 using gcc and size optimisation:
* 3600 Byte of FLASH
* 36 Byte of RAM.

The RAM and FLASH usage doesn't include the commands themselves and the buffers since it is all user defined. 

## Description

### Commands
Commands are grouped in a command table. This help speed up the search and allow the user to load and unload command table
on run time. It also reduces command name conflict. Two command can have the same name as long as they are not in the same table.
It is possible to have commands callable without having to open a table by simply naming the table `""`. See [usage](#command-table).

**Note**: arguments are always separated by a space character.

The console support two ways for the user to execute a command.
1. Enter the table name followed by the command and its arguments. 
```
> table_name command_name arg0 arg1
```
2. Open the table, then call the command and its arguments.
```
> table_name
> [table_name] <- If enabled in the console_options, the current table is shown with prompt.
> [table_name] command_name arg0 arg1
```
The second method is useful when calling multiple command from the same table.

When a table is named `""`, command are called as follows:
```
> command_name arg0 arg1
```
If a table is active, the command is not found.


### Built in commands
The console has built command that can be called all the time:
* `help`: When at root, it prints all the added command table's name. When a table is opened, it prints the table's name and all its commands.
* `quit`: If a command table is opened, return to root. Otherwise, it does nothing.

## Usage

All the console's code is in the namespace `console`.

### Interface
The user must implement the Console_io interface. An example is provided in `example/nucleo-stm32f446re/io_vcp.cpp`

```c++
/* Console interface */
class Console_io: public console::noncopyable {

    public:
        // Open and close I/O.
        virtual void open() = 0;
        virtual void close() = 0;

        // Print string.
        virtual void print(const char* out) = 0;
        virtual void print(const char* out, std::size_t size) = 0;

        // Get first available character. Return EOF (-1) if no key is pressed.
        virtual int get_char() = 0;

        // Destructor
        virtual ~Console_io() = default;
};
```

### Command table
A command table is simply an array of `console::Command`. All command has a name, a function pointer and a help message.
A template function `console::help<bool>(const char*  msg)` is provided to enable of disable the help message at compile
time. This help reduces FLASH usage. 

**IMPORTANT**

The **LAST** item in the array **MUST** be `{console::table_end, nullptr, nullptr}`. When the console scans the table to 
find the received command, it knows the table end when it gets an empty string (`console::table_end`).

```c++
enum class Cmd_result {
    error,
    success
};

typedef Cmd_result (*command_t)(Console_io* io, const std::string_view& input);

struct Command {
    const char*        name;
    console::command_t execute;
    const char*        help;
};
```

Example:
```c++
constexpr bool enable_help = true;
constexpr std::size_t table_size = 2;
constexpr std::array<console::Command, table_size> commands
{{
    {"command",  &cmd, console::help<enable_help>("This is the command help message.")},
    {console::table_end, nullptr, nullptr}
}};
```

The command table needs to be added to the console's instance to be available. All the commands in the table
can be called without having to activate the table in the command line by naming the table with an empty string.
```c++
you_console_instance.add_cmd_table("table_name", commands.data());

// All these commands can be called without having to activate the table.
you_console_instance.add_cmd_table("", commands.data());
```

The table can also be removed with:
```c++
you_console_instance.remove_cmd_table("table_name");
```

### Command
The console provides a parser and functions to convert string to any integral type and vice versa. The console's io and
the full input is provided to user for flexibility. This way, the user can handle the arguments the way they see fit.

Example:
```c++
Cmd_result your_command(Console_io* io, const std::string_view& input)
{
    io->print("\r\n");  // New line to avoid writing right next to the input command.
    io->print("Command write can write on the terminal");
    
    c = io->get_char(); // Can read new input too. A binary argument would be received this way using your 
                        // serialization / deserialization protocol.
    
    // input has the command string minus the table name. 
    // Arguments can be parsed using the console's parser.
    parser::Parser parser(input);
    std::string_view command_name = parser.get_argument(0);
    std::string_view arg1 = parser.get_argument(1);
    
    // Conversion function are provided without using heap.
    // String to integral
    console::conversion::Error error;
    auto value = console::conversion::str_to_integral<std::uint32_t>(arg1, error);
    
    // Integral to str
    constexpr std::uint8_t max_output_size = 8;
    std::array<char, max_output_size> buffer {};

    console::conversion::Format fmt = console::conversion::Format::hex;
    std::size_t size = integral_to_str(input, buffer.begin(), buffer.end(), fmt, error);
    
    io->print(buffer.data(), size);
    
    return Cmd_result::success;
}
```

### Buffers
The console needs an input buffer to store the received characters and a command table buffer to keep added command table.

Since these buffer length can vary a lot from one project to another, they are user defined. It is important to create
and populate `console::input_buffer_info_t` and `console::command_table_buffer_info_t` to pass all the info to the 
console's constructor.

Input buffer example:
```c++
constexpr std::size_t cli_input_buffer_size = 128;
std::array<char, cli_input_buffer_size> cli_input_buffer;
console::input_buffer_info_t cli_input_buffer_info {
    .data = cli_input_buffer.data(),
    .capacity = cli_input_buffer.max_size()
};
```

Table buffer example:
```c++
constexpr std::size_t cli_table_buffer_size = 8;
std::array<console::command_table_entry_t , cli_table_buffer_size> cli_table_buffer;
console::command_table_buffer_info_t cli_table_buffer_info {
    .data = cli_table_buffer.data(),
    .capacity = cli_table_buffer.max_size()
};
```

### Options
The console has multiples console_options. They can be changed on runtime.
```c++
struct Options {
    bool        echo              = true;                // Print each received character.
    bool        show_active_table = true;                // Show active table next to prompt. Ex: > [table_name].
    const char* console_prompt    = "> ";                // Printed at the beginning of each line.
    const char* endline           = "\r\n";              // Printed at the end of each line.
    const char* msg_error         = "Error: ";           // Printed when a command returns error status followed by
                                                         // its help messages.
    const char* msg_cmd_not_found = "Command not found"; // Printed when a command is not found.
};
```
