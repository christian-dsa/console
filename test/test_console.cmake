
find_package(Catch2 REQUIRED)

set(TEST_EXECUTABLE test_${PROJECT_NAME} )
add_executable(${TEST_EXECUTABLE} "")

target_sources(${TEST_EXECUTABLE}
    PRIVATE
    ${CMAKE_CURRENT_LIST_DIR}/catch2_main.cpp
    ${CMAKE_CURRENT_LIST_DIR}/command_table_for_test.cpp
    ${CMAKE_CURRENT_LIST_DIR}/test_console.cpp
    ${CMAKE_CURRENT_LIST_DIR}/test_parser.cpp
    ${CMAKE_CURRENT_LIST_DIR}/test_buffer.cpp
    )

target_include_directories(${TEST_EXECUTABLE} PRIVATE ${CMAKE_CURRENT_LIST_DIR})

target_link_libraries(${TEST_EXECUTABLE}
    PRIVATE
    console
    Catch2::Catch2
    )

target_link_options(${TEST_EXECUTABLE} PRIVATE
    -Wl,-Map=${PROJECT_NAME}.map
    )

target_compile_features(${TEST_EXECUTABLE} PRIVATE cxx_std_17)
