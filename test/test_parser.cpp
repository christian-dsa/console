/**
 * Copyright 2023 Christian Daigneault-St-Arnaud
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions
 *    and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 *    following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
// NOLINTBEGIN
#include "catch2/catch.hpp"
#include "command_parser.h"
#include "console_util.h"
#include <array>
#include <cstring>

using namespace console;

TEST_CASE("Command parser test", "[parser]") {

    SECTION("Parse one parameter") {
        std::string_view input = "CMD_NAME parameter1";
        parser::Parser   parser(input);

        std::string_view output = parser.get_argument(1);
        REQUIRE(output == "parameter1");
    }

    SECTION("Parse first of three parameters") {
        std::string_view input = "CMD_NAME parameter1 parameter2 parameter3";
        parser::Parser   parser(input);

        std::string_view output = parser.get_argument(1);
        REQUIRE(output == "parameter1");
    }

    SECTION("Parse second of three parameters") {
        std::string_view input = "CMD_NAME parameter1 parameter2 parameter3";
        parser::Parser   parser(input);

        std::string_view output = parser.get_argument(2);
        REQUIRE(output == "parameter2");
    }

    SECTION("Parse third of three parameters") {
        std::string_view input = "CMD_NAME parameter1 parameter2 123";
        parser::Parser   parser(input);

        std::string_view output = parser.get_argument(3);
        REQUIRE(output == "123");
    }

    SECTION("Get first of NO parameter") {
        std::string_view input = "CMD_NAME";
        parser::Parser   parser(input);

        std::string_view output = parser.get_argument(1);
        REQUIRE(output.empty());
    }

    SECTION("Get second of NO parameter") {
        std::string_view input = "CMD_NAME";
        parser::Parser   parser(input);

        std::string_view output = parser.get_argument(2);
        REQUIRE(output.empty());
    }

    SECTION("Parameter start but no parameter") {
        std::string_view input = "CMD_NAME ";
        parser::Parser   parser(input);

        std::string_view output = parser.get_argument(1);
        REQUIRE(output.empty());
    }

    SECTION("Get command name") {
        std::string_view input = "CMD_NAME P1 P2";
        parser::Parser   parser(input);

        std::string_view output = parser.get_name();
        REQUIRE(output == "CMD_NAME");
    }

    SECTION("Get command name with extra space on front") {
        std::string_view input = "   CMD_NAME";
        parser::Parser   parser(input);

        std::string_view output = parser.get_name();
        REQUIRE(output == "CMD_NAME");
    }

    SECTION("Get parameter with extra space on front") {
        std::string_view input = "CMD_NAME               parameter1";
        parser::Parser   parser(input);

        std::string_view output = parser.get_argument(1);
        REQUIRE(output == "parameter1");
    }

    SECTION("Get parameter 0") {
        std::string_view input = "CMD_NAME   parameter1";
        parser::Parser   parser(input);

        std::string_view output = parser.get_argument(0);
        REQUIRE(output == "CMD_NAME");
    }

    SECTION("Get parameter 0 with parameter start at beginning") {
        std::string_view input = "  CMD_NAME   parameter1";
        parser::Parser   parser(input);

        std::string_view output = parser.get_argument(0);
        REQUIRE(output == "CMD_NAME");
    }

    SECTION("Get command name with string ending with new line char") {
        std::string_view input = "CMD_NAME\n";
        parser::Parser   parser(input);

        std::string_view output = parser.get_argument(0);
        REQUIRE(output == "CMD_NAME");
    }
}

TEST_CASE("String to integral, hex value", "[hex parser]") {

    SECTION("Parse hex value 0xFACE") {
        std::string_view input = "CMD_NAME 0xFACE ";
        parser::Parser   parser(input);

        std::string_view output = parser.get_argument(1);

        console::conversion::Error error;
        auto                       value = console::conversion::str_to_integral<std::uint32_t>(output, error);

        REQUIRE(value == 0xFACE);
    }

    SECTION("Parse hex value 0x12") {
        std::string_view input = "CMD_NAME 0x12 ";
        parser::Parser   parser(input);

        std::string_view output = parser.get_argument(1);

        console::conversion::Error error;
        auto                       value = console::conversion::str_to_integral<std::uint32_t>(output, error);

        REQUIRE(value == 0x12);
    }

    SECTION("Parse invalid hex value 0x12345678") {
        std::string_view input = "CMD_NAME 0x123F45678 ";
        parser::Parser   parser(input);

        std::string_view output = parser.get_argument(1);

        console::conversion::Error error;
        auto                       value = console::conversion::str_to_integral<std::uint8_t>(output, error);

        REQUIRE(value == 0);
        REQUIRE(error == console::conversion::Error::out_of_range);
    }

    SECTION("Parse invalid hex value xHELLO") {
        std::string_view input = "CMD_NAME xHELLO_WORLD ";
        parser::Parser   parser(input);

        std::string_view output = parser.get_argument(1);

        console::conversion::Error error;
        auto                       value = console::conversion::str_to_integral<std::uint32_t>(output, error);

        REQUIRE(value == 0);
        REQUIRE(error == console::conversion::Error::invalid_input);
    }

    SECTION("Parse hex to float") {
        std::string_view input = "CMD_NAME  0x34";
        parser::Parser   parser(input);

        std::string_view output = parser.get_argument(1);

        /*
        console::conversion::Error error;
        auto value = console::conversion::str_to_integral<float>(output, error);
        */
        // Does not compile!
        (void)output;
    }
}

TEST_CASE("String to integral, dec value", "[dec parser]") {

    SECTION("Parse dec value 345") {
        std::string_view input = "CMD_NAME 345 ";
        parser::Parser   parser(input);

        std::string_view output = parser.get_argument(1);

        console::conversion::Error error;
        auto                       value = console::conversion::str_to_integral<std::uint32_t>(output, error);

        REQUIRE(value == 345);
    }

    SECTION("Parse dec value 345 in a std::uint8_t") {
        std::string_view input = "CMD_NAME 345 ";
        parser::Parser   parser(input);

        std::string_view output = parser.get_argument(1);

        console::conversion::Error error;
        auto                       value = console::conversion::str_to_integral<std::uint8_t>(output, error);

        REQUIRE(value == 0);
        REQUIRE(error == console::conversion::Error::out_of_range);
    }

    SECTION("Parse dec value -345") {
        std::string_view input = "CMD_NAME -345 ";
        parser::Parser   parser(input);

        std::string_view output = parser.get_argument(1);

        console::conversion::Error error;
        auto                       value = console::conversion::str_to_integral<std::int32_t>(output, error);

        REQUIRE(value == -345);
    }

    SECTION("Parse dec value 12e") {
        std::string_view input = "CMD_NAME 12e ";
        parser::Parser   parser(input);

        std::string_view output = parser.get_argument(1);

        console::conversion::Error error;
        auto                       value = console::conversion::str_to_integral<std::uint32_t>(output, error);

        REQUIRE(value == 0);
        REQUIRE(error == console::conversion::Error::invalid_input);
    }

    SECTION("Parse dec value (empty)") {
        std::string_view input = "CMD_NAME ";
        parser::Parser   parser(input);

        std::string_view output = parser.get_argument(1);

        console::conversion::Error error;
        auto                       value = console::conversion::str_to_integral<std::uint32_t>(output, error);

        REQUIRE(value == 0);
        REQUIRE(error == console::conversion::Error::invalid_input);
    }
}

TEST_CASE("String to integral, binary value", "[bin parser]") {

    SECTION("Parse bin value 0b01011010") {
        std::string_view input = "CMD_NAME 0b01011010 ";
        parser::Parser   parser(input);

        std::string_view output = parser.get_argument(1);

        console::conversion::Error error;
        auto                       value = console::conversion::str_to_integral<std::uint32_t>(output, error);

        REQUIRE(value == 0b01011010);
    }

    SECTION("Parse bin value -0b01011010") {
        std::string_view input = "CMD_NAME -0b01011010 ";
        parser::Parser   parser(input);

        std::string_view output = parser.get_argument(1);

        console::conversion::Error error;
        auto                       value = console::conversion::str_to_integral<std::uint32_t>(output, error);

        REQUIRE(value == 0);
        REQUIRE(error == console::conversion::Error::invalid_input);
    }

    SECTION("Parse bin value 0b0101101001011010") {
        std::string_view input = "CMD_NAME 0b0101101001011010";
        parser::Parser   parser(input);

        std::string_view output = parser.get_argument(1);

        console::conversion::Error error;
        auto                       value = console::conversion::str_to_integral<std::uint8_t>(output, error);

        REQUIRE(value == 0);
        REQUIRE(error == console::conversion::Error::out_of_range);
    }
}

TEST_CASE("Hex to string conversion", "[hex to str]") {
    constexpr std::uint8_t            max_output_size = 8;
    std::array<char, max_output_size> buffer{};
    std::fill(buffer.begin(), buffer.end(), 127);

    console::conversion::Error  error;
    console::conversion::Format fmt = console::conversion::Format::hex;

    SECTION("Print value 0x23") {
        std::uint32_t input = 0x23;

        std::size_t size = integral_to_str(input, buffer.begin(), buffer.end(), fmt, error);

        const char* expected = "0x23";
        REQUIRE(strcmp(buffer.begin(), expected) == 0);
        REQUIRE(size == strlen(expected));
    }

    SECTION("Print value 0x01234567890 while buffer is too small") {
        std::uint64_t input = 0x01234567890;

        std::size_t size = integral_to_str(input, buffer.begin(), buffer.end(), fmt, error);

        const char* expected = "";
        REQUIRE(error == console::conversion::Error::destination_too_small);
        REQUIRE(size == strlen(expected));
    }

    SECTION("Print value 0x01 but receive an output buffer of size 0") {
        std::uint64_t input = 0x01;
        char          micro_buffer[0];

        std::size_t size = integral_to_str(input, micro_buffer, micro_buffer, fmt, error);

        const char* expected = "";
        REQUIRE(error == console::conversion::Error::destination_too_small);
        REQUIRE(size == strlen(expected));
    }
}

TEST_CASE("Dec to string conversion", "[dec to str]") {
    constexpr std::uint8_t            max_output_size = 8;
    std::array<char, max_output_size> buffer{};
    std::fill(buffer.begin(), buffer.end(), 127);

    console::conversion::Error  error;
    console::conversion::Format fmt = console::conversion::Format::dec;

    SECTION("Print value 23") {
        std::uint32_t input = 23;

        std::size_t size = integral_to_str(input, buffer.begin(), buffer.end(), fmt, error);

        const char* expected = "23";
        REQUIRE(strcmp(buffer.begin(), expected) == 0);
        REQUIRE(size == strlen(expected));
    }

    SECTION("Print value 4294967296 while buffer is too small") {
        std::uint64_t input = 4294967296;

        std::size_t size = integral_to_str(input, buffer.begin(), buffer.end(), fmt, error);

        const char* expected = "";
        REQUIRE(error == console::conversion::Error::destination_too_small);
        REQUIRE(size == strlen(expected));
    }

    SECTION("Print value 0 but receive an output buffer of size 0") {
        std::uint64_t input = 0;
        char          micro_buffer[0];

        std::size_t size = integral_to_str(input, micro_buffer, micro_buffer, fmt, error);

        const char* expected = "";
        REQUIRE(error == console::conversion::Error::destination_too_small);
        REQUIRE(size == strlen(expected));
    }
}

TEST_CASE("Bin to string conversion", "[bin to str]") {
    constexpr std::uint8_t            max_output_size = 8;
    std::array<char, max_output_size> buffer{};
    std::fill(buffer.begin(), buffer.end(), 127);

    console::conversion::Error  error;
    console::conversion::Format fmt = console::conversion::Format::bin;

    SECTION("Print value 0b0111") {
        std::uint32_t input = 0b0111;

        std::size_t size = integral_to_str(input, buffer.begin(), buffer.end(), fmt, error);

        const char* expected = "0b111";  // Doesn't keep leading zeros.
        REQUIRE(strcmp(buffer.begin(), expected) == 0);
        REQUIRE(size == strlen(expected));
    }

    SECTION("Print value 0b10010110101001010110 while buffer is too small") {
        std::uint64_t input = 0b10010110101001010110;

        std::size_t size = integral_to_str(input, buffer.begin(), buffer.end(), fmt, error);

        const char* expected = "";
        REQUIRE(error == console::conversion::Error::destination_too_small);
        REQUIRE(size == strlen(expected));
    }

    SECTION("Print value 0 but receive an output buffer of size 0") {
        std::uint64_t input = 0;
        char          micro_buffer[0];

        std::size_t size = integral_to_str(input, micro_buffer, micro_buffer, fmt, error);

        const char* expected = "";
        REQUIRE(error == console::conversion::Error::destination_too_small);
        REQUIRE(size == strlen(expected));
    }
}
// NOLINTEND
