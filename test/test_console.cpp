/**
 * Copyright 2023 Christian Daigneault-St-Arnaud
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions
 *    and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 *    following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
// NOLINTBEGIN
#include "catch2/catch.hpp"
#include "command_table_for_test.h"
#include "console.h"
#include "mock_io.h"
#include <array>

using namespace console;

SCENARIO("Command's help message", "[util]") {

    SECTION("Help enable") {
        std::string help_msg;

        help_msg = help<true>("help msg enabled");
        REQUIRE(help_msg == "help msg enabled");
    }

    SECTION("Help disabled") {
        std::string help_msg;

        help_msg = help<false>("help msg disabled");
        REQUIRE(help_msg.empty());
    }
}

// Test helper function prototypes
void run_process(Console& cli, std::uint32_t nb_time);

SCENARIO("Console", "[console]") {

    constexpr std::size_t input_buffer_size         = 24;
    constexpr std::size_t command_table_buffer_size = 4;

    std::array<char, input_buffer_size> input_memory{};

    input_buffer_info_t input_buffer_info{.data = input_memory.data(), .capacity = input_memory.size()};

    std::array<command_table_entry_t, command_table_buffer_size> table_memory{};

    command_table_buffer_info_t command_table_buffer_info{.data = table_memory.data(), .capacity = table_memory.size()};

    Mock_io io;
    io.open();

    std::string endline = "\r\n";
    std::string prompt  = ">";

    GIVEN("Console is initialized") {
        Options cli_options{
            .echo              = true,
            .console_prompt    = prompt.c_str(),
            .endline           = endline.c_str(),
            .msg_error         = "Error: ",
            .msg_cmd_not_found = "Command not found",
        };

        Console cli(io, input_buffer_info, command_table_buffer_info, cli_options);

        WHEN("start console with empty message") {
            io.get_char_val = {EOF};

            cli.start("");

            THEN("two newline and the prompt are printed") {
                REQUIRE(io.print_history.size() == 4);
                REQUIRE(io.print_history[0] == "\r\n");
                REQUIRE(io.print_history[1].empty());
                REQUIRE(io.print_history[2] == "\r\n");
                REQUIRE(io.print_history[3] == ">");
            }
        }

        WHEN("start console with message") {
            io.get_char_val = {EOF};

            cli.start("Hello world");

            THEN("two newline and the prompt are printed") {
                REQUIRE(io.print_history.size() == 4);
                REQUIRE(io.print_history[0] == "\r\n");
                REQUIRE(io.print_history[1] == "Hello world");
                REQUIRE(io.print_history[2] == "\r\n");
                REQUIRE(io.print_history[3] == ">");
            }
        }
    }

    GIVEN("Console is started, echo is ON") {
        Options cli_options{
            .echo              = true,
            .show_active_table = false,
            .console_prompt    = prompt.c_str(),
            .endline           = endline.c_str(),
            .msg_error         = "Error: ",
            .msg_cmd_not_found = "Command not found",
        };

        Console cli(io, input_buffer_info, command_table_buffer_info, cli_options);
        cli.start("");
        io.reset();

        WHEN("user write text on the terminal") {
            io.get_char_val = {'c', 'm', 'd', '0', EOF};
            cli.process();

            THEN("all characters are printed") {
                REQUIRE(io.get_char_index == 5);
                REQUIRE(io.print_history.size() == 4);
                REQUIRE(io.print_history[0] == "c");
                REQUIRE(io.print_history[1] == "m");
                REQUIRE(io.print_history[2] == "d");
                REQUIRE(io.print_history[3] == "0");
            }
        }

        WHEN("echo is disabled on runtime") {
            cli_options.echo = false;
            cli.set_options(cli_options);

            THEN("no character is printed on the terminal when user type")
            io.get_char_val = {'c', 'm', 'd', '0', EOF};
            cli.process();

            REQUIRE(io.get_char_index == 5);
            REQUIRE(io.print_history.empty());
        }

        WHEN("Enter delete many times while input buffer is empty") {
            io.get_char_val = {8, EOF, 127, EOF, 8, 127, 8, EOF};
            run_process(cli, 4);

            THEN("nothing happens") {
                REQUIRE(io.print_history.empty());
            }
        }

        WHEN("Enter arrow key") {
            // Arrow keys are ignored
            io.get_char_val = {0x1B, EOF, 0x5B, 0x41, EOF};  // Cursor up
            run_process(cli, 3);

            THEN("nothing happens") {
                REQUIRE(io.print_history.empty());
            }
        }

        WHEN("Enter page down key") {
            io.get_char_val = {0x1B, 0x5B, EOF, 0x35, EOF, 0x7E, EOF};  // Cursor up
            run_process(cli, 3);

            THEN("nothing happens") {
                REQUIRE(io.print_history.empty());
            }
        }

        WHEN("LF character is received multiple times") {
            io.get_char_val = {'\r', '\r', EOF, '\r', 'x', EOF};
            run_process(cli, 8);

            THEN("a new line is printed with a prompt each time LF char is received") {
                REQUIRE(io.print_history.size() == 7);
                REQUIRE(io.print_history[0] == endline);
                REQUIRE(io.print_history[1] == prompt);
                REQUIRE(io.print_history[2] == endline);
                REQUIRE(io.print_history[3] == prompt);
                REQUIRE(io.print_history[4] == endline);
                REQUIRE(io.print_history[5] == prompt);
                REQUIRE(io.print_history[6] == "x");
            }
        }

        WHEN("CR character is received multiple times") {
            io.get_char_val = {'\n', EOF, '\n', '\n', 'x', EOF};
            run_process(cli, 8);

            THEN("a new line is printed with a prompt each time CR char is received") {
                REQUIRE(io.print_history.size() == 7);
                REQUIRE(io.print_history[0] == endline);
                REQUIRE(io.print_history[1] == prompt);
                REQUIRE(io.print_history[2] == endline);
                REQUIRE(io.print_history[3] == prompt);
                REQUIRE(io.print_history[4] == endline);
                REQUIRE(io.print_history[5] == prompt);
                REQUIRE(io.print_history[6] == "x");
            }
        }

        WHEN("CR+LF characters are received multiple times") {
            io.get_char_val = {'\r', '\n', EOF, '\r', '\n', '\r', '\n', 'x', EOF};
            run_process(cli, 8);

            THEN("a new line is printed with a prompt each time CR+LF characters are received") {
                REQUIRE(io.print_history.size() == 7);
                REQUIRE(io.print_history[0] == endline);
                REQUIRE(io.print_history[1] == prompt);
                REQUIRE(io.print_history[2] == endline);
                REQUIRE(io.print_history[3] == prompt);
                REQUIRE(io.print_history[4] == endline);
                REQUIRE(io.print_history[5] == prompt);
                REQUIRE(io.print_history[6] == "x");
            }
        }

        WHEN("user press backspace key while typing text") {
            io.get_char_val = {'n', 'c', 'm', 8, 127, 'd', EOF};
            cli.process();

            THEN("backspace erase the previous character on the terminal") {
                REQUIRE(io.print_history.size() == 6);
                REQUIRE(io.print_history[0] == "n");
                REQUIRE(io.print_history[1] == "c");
                REQUIRE(io.print_history[2] == "m");
                REQUIRE(io.print_history[3] == "\b \b");
                REQUIRE(io.print_history[4] == "\b \b");
                REQUIRE(io.print_history[5] == "d");
            }
        }

        WHEN("change console's io in runtime") {
            Mock_io new_io;

            // Process with io
            io.get_char_val = {'s', 't', 'a', 'r', 't', EOF};
            run_process(cli, 1);

            // Process with new_io
            new_io.reset();
            new_io.get_char_val = {'e', 'n', 'd', EOF};

            cli.set_interface(new_io);
            run_process(cli, 1);

            THEN("echo went from one io to the other") {
                REQUIRE(io.print_history.size() == 5);
                REQUIRE(io.print_history[0] == "s");
                REQUIRE(io.print_history[1] == "t");
                REQUIRE(io.print_history[2] == "a");
                REQUIRE(io.print_history[3] == "r");
                REQUIRE(io.print_history[4] == "t");

                REQUIRE(new_io.print_history.size() == 3);
                REQUIRE(new_io.print_history[0] == "e");
                REQUIRE(new_io.print_history[1] == "n");
                REQUIRE(new_io.print_history[2] == "d");
            }
        }
    }

    GIVEN("Console is started, echo is OFF. no command table") {
        Options cli_options{
            .echo              = false,
            .show_active_table = false,
            .console_prompt    = prompt.c_str(),
            .endline           = endline.c_str(),
            .msg_error         = "Error: ",
            .msg_cmd_not_found = "Command not found",
        };

        Console cli(io, input_buffer_info, command_table_buffer_info, cli_options);
        cli.start("");
        io.reset();

        WHEN("user write text on the terminal") {
            io.get_char_val = {'c', 'm', 'd', '0', EOF};
            cli.process();

            THEN("no character is printed on the terminal") {
                REQUIRE(io.get_char_index == 5);
                REQUIRE(io.print_history.empty());
            }
        }

        WHEN("echo is enabled on runtime") {
            cli_options.echo = true;
            cli.set_options(cli_options);

            THEN("all characters are printed when user type") {
                io.get_char_val = {'c', 'm', 'd', '0', EOF};
                cli.process();

                REQUIRE(io.get_char_index == 5);
                REQUIRE(io.print_history.size() == 4);
                REQUIRE(io.print_history[0] == "c");
                REQUIRE(io.print_history[1] == "m");
                REQUIRE(io.print_history[2] == "d");
                REQUIRE(io.print_history[3] == "0");
            }
        }

        WHEN("user enter text and press ENTER") {
            io.get_char_val = {'n', 'c', 'm', 'd', '\n', EOF};
            cli.process();

            THEN("user receives Command not found") {
                REQUIRE(io.print_history.size() == 4);
                REQUIRE(io.print_history[0] == endline);
                REQUIRE(io.print_history[1] == "Command not found");
                REQUIRE(io.print_history[2] == endline);
                REQUIRE(io.print_history[3] == prompt);
            }
        }

        WHEN("user enter too many characters, input buffer overflow") {
            // clang-format off
            io.get_char_val = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
                               'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't',
                               'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', EOF,
                               'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', EOF,
                               'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', '\n', EOF, // Command not found, print prompt, reset input
                               '0', '1', '2', '3', '4', '5', '6', '7', '8', '\n', EOF, // Command not found, print prompt
            };
            // clang-format on
            run_process(cli, 6);

            THEN("even with an overflow, it catches the newline character and handle the command") {
                REQUIRE(io.print_history.size() == 8);
                REQUIRE(io.print_history[0] == endline);
                REQUIRE(io.print_history[1] == "Command not found");
                REQUIRE(io.print_history[2] == endline);
                REQUIRE(io.print_history[3] == prompt);
                REQUIRE(io.print_history[4] == endline);
                REQUIRE(io.print_history[5] == "Command not found");
                REQUIRE(io.print_history[6] == endline);
                REQUIRE(io.print_history[7] == prompt);
            }
        }
    }

    GIVEN("Console is started, echo is OFF. Add t0_table (help disabled) as root table") {
        Options cli_options{
            .echo              = false,
            .show_active_table = false,
            .console_prompt    = prompt.c_str(),
            .endline           = endline.c_str(),
            .msg_error         = "Error: ",
            .msg_cmd_not_found = "Command not found",
        };

        Console cli(io, input_buffer_info, command_table_buffer_info, cli_options);
        cli.start("");
        cli.add_cmd_table("", t0_cmd::get_t0_table());
        io.reset();

        WHEN("enter valid command cmd0") {
            io.get_char_val = {EOF, 'c', EOF, 'm', EOF, EOF, EOF, 'd', '0', EOF, '\r', EOF};
            run_process(cli, 12);

            THEN("cmd0 is called") {
                REQUIRE(io.print_history.size() == 4);
                REQUIRE(io.print_history[0] == endline);
                REQUIRE(io.print_history[1] == "cmd0 called");
                REQUIRE(io.print_history[2] == endline);
                REQUIRE(io.print_history[3] == prompt);
            }
        }

        WHEN("enter valid command with parameter") {
            io.get_char_val = {'c', 'm', 'd', '0', ' ', 'p', '1', '\r', EOF};
            cli.process();

            THEN("cmd0 is called") {
                REQUIRE(io.print_history.size() == 4);
                REQUIRE(io.print_history[0] == endline);
                REQUIRE(io.print_history[1] == "cmd0 called");
                REQUIRE(io.print_history[2] == endline);
                REQUIRE(io.print_history[3] == prompt);
            }
        }

        WHEN("enter invalid command") {
            io.get_char_val = {'n', 'c', 'm', 'd', '\n', EOF};
            cli.process();

            THEN("user receives Command not found") {
                REQUIRE(io.print_history.size() == 4);
                REQUIRE(io.print_history[0] == endline);
                REQUIRE(io.print_history[1] == "Command not found");
                REQUIRE(io.print_history[2] == endline);
                REQUIRE(io.print_history[3] == prompt);
            }
        }

        WHEN("enter valid command, command return Error (this is application specific)") {
            io.get_char_val = {'c', 'm', 'd', '4', '\r', EOF};
            cli.process();

            THEN("console print empty help message since help is disabled") {
                REQUIRE(io.print_history.size() == 7);
                REQUIRE(io.print_history[0] == endline);
                REQUIRE(io.print_history[1] == "cmd4 called");
                REQUIRE(io.print_history[2] == endline);
                REQUIRE(io.print_history[3] == "Error: ");
                REQUIRE(io.print_history[4].empty());
                REQUIRE(io.print_history[5] == endline);
                REQUIRE(io.print_history[6] == prompt);
            }
        }
    }

    GIVEN("Console is started, echo is OFF. Add t1_table (help enabled) as root table") {
        Options cli_options{
            .echo              = false,
            .show_active_table = false,
            .console_prompt    = prompt.c_str(),
            .endline           = endline.c_str(),
            .msg_error         = "Error: ",
            .msg_cmd_not_found = "Command not found",
        };

        Console cli(io, input_buffer_info, command_table_buffer_info, cli_options);
        cli.start("");
        cli.add_cmd_table("", t1_cmd::get_t1_table());
        io.reset();

        WHEN("enter valid command, command return Error (this is application specific)") {
            io.get_char_val = {'t', '1', 'c', 'm', 'd', '1', '\r', EOF};
            cli.process();

            THEN("console print help message") {
                REQUIRE(io.print_history.size() == 7);
                REQUIRE(io.print_history[0] == endline);
                REQUIRE(io.print_history[1] == "t1cmd1 called");
                REQUIRE(io.print_history[2] == endline);
                REQUIRE(io.print_history[3] == "Error: ");
                REQUIRE(io.print_history[4] == "t1cmd1 help");
                REQUIRE(io.print_history[5] == endline);
                REQUIRE(io.print_history[6] == prompt);
            }
        }

        WHEN("enter help") {
            io.get_char_val = {'h', 'e', 'l', 'p', '\n', EOF};
            cli.process();

            THEN("print all available commands in root table") {
                REQUIRE(io.print_history.size() == 21);
                REQUIRE(io.print_history[0] == endline);
                REQUIRE(io.print_history[1].empty());
                REQUIRE(io.print_history[2] == endline);
                REQUIRE(io.print_history[3] == "t1cmd0");
                REQUIRE(io.print_history[4] == " : ");
                REQUIRE(io.print_history[5] == "t1cmd0 help");
                REQUIRE(io.print_history[6] == endline);
                REQUIRE(io.print_history[7] == "t1cmd1");
                REQUIRE(io.print_history[8] == " : ");
                REQUIRE(io.print_history[9] == "t1cmd1 help");
                REQUIRE(io.print_history[10] == endline);
                REQUIRE(io.print_history[11] == "1");
                REQUIRE(io.print_history[12] == " ");
                REQUIRE(io.print_history[13] == " ");
                REQUIRE(io.print_history[14] == " ");
                REQUIRE(io.print_history[15] == " ");
                REQUIRE(io.print_history[16] == " ");
                REQUIRE(io.print_history[17] == " : ");
                REQUIRE(io.print_history[18] == "1 help");
                REQUIRE(io.print_history[19] == endline);
                REQUIRE(io.print_history[20] == prompt);
            }
        }
    }

    GIVEN("Console is started, echo is OFF. Command table t1_table as App1 table is added and activated, show active "
          "table is ON") {
        Options cli_options{
            .echo              = false,
            .show_active_table = true,
            .console_prompt    = prompt.c_str(),
            .endline           = endline.c_str(),
            .msg_error         = "Error: ",
            .msg_cmd_not_found = "Command not found",
        };

        Console cli(io, input_buffer_info, command_table_buffer_info, cli_options);
        cli.add_cmd_table("App1 table", t1_cmd::get_t1_table());  // Adding table before cli.start activate it since
                                                                  // it is the only one.
        cli.start("");
        io.reset();

        WHEN("user start a new line") {
            io.get_char_val = {'\n', EOF};
            cli.process();

            THEN("start a new line with prompt and table name") {
                REQUIRE(io.print_history.size() == 5);
                REQUIRE(io.print_history[0] == endline);
                REQUIRE(io.print_history[1] == prompt);
                REQUIRE(io.print_history[2] == "[");
                REQUIRE(io.print_history[3] == "App1 table");
                REQUIRE(io.print_history[4] == "] ");
            }
        }

        WHEN("enter help") {
            io.get_char_val = {'h', 'e', 'l', 'p', '\n', EOF};
            cli.process();

            THEN("all command from App1 table are printed with their help message") {
                REQUIRE(io.print_history.size() == 24);
                REQUIRE(io.print_history[0] == endline);
                REQUIRE(io.print_history[1] == "App1 table");
                REQUIRE(io.print_history[2] == endline);
                REQUIRE(io.print_history[3] == "t1cmd0");
                REQUIRE(io.print_history[4] == " : ");
                REQUIRE(io.print_history[5] == "t1cmd0 help");
                REQUIRE(io.print_history[6] == endline);
                REQUIRE(io.print_history[7] == "t1cmd1");
                REQUIRE(io.print_history[8] == " : ");
                REQUIRE(io.print_history[9] == "t1cmd1 help");
                REQUIRE(io.print_history[10] == endline);
                REQUIRE(io.print_history[11] == "1");
                REQUIRE(io.print_history[12] == " ");
                REQUIRE(io.print_history[13] == " ");
                REQUIRE(io.print_history[14] == " ");
                REQUIRE(io.print_history[15] == " ");
                REQUIRE(io.print_history[16] == " ");
                REQUIRE(io.print_history[17] == " : ");
                REQUIRE(io.print_history[18] == "1 help");
                REQUIRE(io.print_history[19] == endline);
                REQUIRE(io.print_history[20] == prompt);
                REQUIRE(io.print_history[21] == "[");
                REQUIRE(io.print_history[22] == "App1 table");
                REQUIRE(io.print_history[23] == "] ");
            }
        }
    }

    GIVEN("Console is started, echo is OFF. Command table t1_table as App1 table is added and activated, show active "
          "table is OFF") {
        Options cli_options{
            .echo              = false,
            .show_active_table = false,
            .console_prompt    = prompt.c_str(),
            .endline           = endline.c_str(),
            .msg_error         = "Error: ",
            .msg_cmd_not_found = "Command not found",
        };

        Console cli(io, input_buffer_info, command_table_buffer_info, cli_options);
        cli.add_cmd_table("App1 table", t1_cmd::get_t1_table());  // Adding table before cli.start activate it since
                                                                  // it is the only one.
        cli.start("");
        io.reset();

        WHEN("user start a new line") {
            io.get_char_val = {'\n', EOF};
            cli.process();

            THEN("start a new line with only the prompt") {
                REQUIRE(io.print_history.size() == 2);
                REQUIRE(io.print_history[0] == endline);
                REQUIRE(io.print_history[1] == prompt);
            }
        }
    }

    GIVEN("Console is started, echo is OFF. Multiple tables are added, show active table is ON") {
        Options cli_options{
            .echo              = false,
            .show_active_table = true,
            .console_prompt    = prompt.c_str(),
            .endline           = endline.c_str(),
            .msg_error         = "Error: ",
            .msg_cmd_not_found = "Command not found",
        };

        Console cli(io, input_buffer_info, command_table_buffer_info, cli_options);

        // To validate help command, do not place command table in alphabetical order
        cli.add_cmd_table("T2", t2_cmd::get_t2_table());
        cli.add_cmd_table("T0", t0_cmd::get_t0_table());
        cli.add_cmd_table("T1", t1_cmd::get_t1_table());

        cli.start("");
        io.reset();

        WHEN("user open table T1") {
            io.get_char_val = {'T', '1', '\n', EOF};
            cli.process();

            THEN("T1 table is opened.") {
                REQUIRE(io.print_history.size() == 5);
                REQUIRE(io.print_history[0] == endline);
                REQUIRE(io.print_history[1] == prompt);
                REQUIRE(io.print_history[2] == "[");
                REQUIRE(io.print_history[3] == "T1");
                REQUIRE(io.print_history[4] == "] ");
            }
        }

        WHEN("T1 table is removed") {
            cli.remove_cmd_table("T1");

            THEN("it cannot be opened") {
                // Go to T1
                io.get_char_val = {'T', '1', '\n', EOF};
                cli.process();

                REQUIRE(io.print_history.size() == 4);
                REQUIRE(io.print_history[0] == endline);
                REQUIRE(io.print_history[1] == "Command not found");
                REQUIRE(io.print_history[2] == endline);
                REQUIRE(io.print_history[3] == prompt);
            }
        }

        WHEN("a table that doesn't exists is removed") {
            cli.remove_cmd_table("T5");  // Remove T5 which doesn't exist

            THEN("console doesn't crash") {
                io.get_char_val = {'\n', EOF};
                cli.process();
            }
        }

        WHEN("user enter help command when no table is selected") {
            io.get_char_val = {'h', 'e', 'l', 'p', '\n', EOF};
            cli.process();

            THEN("table's name are printed in alphabetical order") {
                REQUIRE(io.print_history.size() == 8);
                REQUIRE(io.print_history[0] == endline);
                REQUIRE(io.print_history[1] == "T0");
                REQUIRE(io.print_history[2] == endline);
                REQUIRE(io.print_history[3] == "T1");
                REQUIRE(io.print_history[4] == endline);
                REQUIRE(io.print_history[5] == "T2");
                REQUIRE(io.print_history[6] == endline);
                REQUIRE(io.print_history[7] == prompt);
            }
        }

        WHEN("a table is removed") {
            cli.remove_cmd_table("T1");

            THEN("help command show the table is not there anymore") {
                io.get_char_val = {'h', 'e', 'l', 'p', '\n', EOF};
                cli.process();

                REQUIRE(io.print_history.size() == 6);
                REQUIRE(io.print_history[0] == endline);
                REQUIRE(io.print_history[1] == "T0");
                REQUIRE(io.print_history[2] == endline);
                REQUIRE(io.print_history[3] == "T2");
                REQUIRE(io.print_history[4] == endline);
                REQUIRE(io.print_history[5] == prompt);
            }
        }

        WHEN("too many command table are added") {
            cli.add_cmd_table("T3", t0_cmd::get_t0_table());
            cli.add_cmd_table("T4", t1_cmd::get_t1_table());
            cli.add_cmd_table("T5", t2_cmd::get_t2_table());

            THEN("all table added when buffer is full are ignored") {
                io.get_char_val = {'h', 'e', 'l', 'p', '\n', EOF};
                cli.process();

                REQUIRE(io.print_history.size() == 10);
                REQUIRE(io.print_history[0] == endline);
                REQUIRE(io.print_history[1] == "T0");
                REQUIRE(io.print_history[2] == endline);
                REQUIRE(io.print_history[3] == "T1");
                REQUIRE(io.print_history[4] == endline);
                REQUIRE(io.print_history[5] == "T2");
                REQUIRE(io.print_history[6] == endline);
                REQUIRE(io.print_history[7] == "T3");
                REQUIRE(io.print_history[8] == endline);
                REQUIRE(io.print_history[9] == prompt);
            }
        }

        WHEN("execute command by giving table name and command") {
            // In this case, the table name is a command name and the command a sub command
            // The command table isn't activated, console stays at root.
            io.get_char_val = {'T', '1', ' ', ' ', 't', '1', 'c', 'm', 'd', '0', '\n', EOF};
            cli.process();

            THEN("command is called and no table is active") {
                REQUIRE(io.print_history.size() == 4);
                REQUIRE(io.print_history[0] == endline);
                REQUIRE(io.print_history[1] == "t1cmd0 called");
                REQUIRE(io.print_history[2] == endline);
                REQUIRE(io.print_history[3] == prompt);
            }
        }

        WHEN("execute command that doesn't exists by giving table name and command, no table is currently active") {
            // In this case, the table name is a command name and the command a sub command
            // The command table isn't activated, console stays at root.
            io.get_char_val = {'T', '1', ' ', 'n', 'o', 'c', 'm', 'd', '0', '\n', EOF};
            cli.process();

            THEN("command is not found and no table is active") {
                REQUIRE(io.print_history.size() == 4);
                REQUIRE(io.print_history[0] == endline);
                REQUIRE(io.print_history[1] == "Command not found");
                REQUIRE(io.print_history[2] == endline);
                REQUIRE(io.print_history[3] == prompt);
            }
        }

        WHEN("execute command from code") {
            Cmd_result result = cli.execute("T1 t1cmd0");

            THEN("command is executed") {
                REQUIRE(io.print_history.size() == 2);
                REQUIRE(io.print_history[0] == endline);
                REQUIRE(io.print_history[1] == "t1cmd0 called");
                REQUIRE(result == Cmd_result::success);
            }
        }

        WHEN("execute a command that doesn't exists from code") {
            Cmd_result result = cli.execute("not1cmd0");

            THEN("nothing happens") {
                REQUIRE(io.print_history.empty());
                REQUIRE(result == Cmd_result::error);
            }
        }
    }

    GIVEN("Console is started, echo is OFF. Multiple tables & root table are added, show active table is ON. T1 is "
          "active") {
        Options cli_options{
            .echo              = false,
            .show_active_table = true,
            .console_prompt    = prompt.c_str(),
            .endline           = endline.c_str(),
            .msg_error         = "Error: ",
            .msg_cmd_not_found = "Command not found",
        };

        Console cli(io, input_buffer_info, command_table_buffer_info, cli_options);
        cli.add_cmd_table("", root_cmd::get_root_table());
        cli.add_cmd_table("T0", t0_cmd::get_t0_table());
        cli.add_cmd_table("T1", t1_cmd::get_t1_table());
        cli.add_cmd_table("T2", t2_cmd::get_t2_table());
        cli.start("");

        io.get_char_val = {'T', '1', '\n', EOF};
        cli.process();
        io.reset();

        WHEN("call command in T1") {
            io.get_char_val = {'t', '1', 'c', 'm', 'd', '0', '\n', EOF};
            cli.process();

            THEN("command is executed") {
                REQUIRE(io.print_history.size() == 7);
                REQUIRE(io.print_history[0] == endline);
                REQUIRE(io.print_history[1] == "t1cmd0 called");
                REQUIRE(io.print_history[2] == endline);
                REQUIRE(io.print_history[3] == prompt);
                REQUIRE(io.print_history[4] == "[");
                REQUIRE(io.print_history[5] == "T1");
                REQUIRE(io.print_history[6] == "] ");
            }
        }

        WHEN("T1 table is removed") {
            cli.remove_cmd_table("T1");

            THEN("it closed, console is now at root") {
                io.get_char_val = {'t', '1', 'c', 'm', 'd', '0', '\n', EOF};
                cli.process();

                REQUIRE(io.print_history.size() == 4);
                REQUIRE(io.print_history[0] == endline);
                REQUIRE(io.print_history[1] == "Command not found");
                REQUIRE(io.print_history[2] == endline);
                REQUIRE(io.print_history[3] == prompt);
            }
        }

        WHEN("execute command in root table") {
            io.get_char_val = {'r', 'o', 'o', 't', '_', 'c', 'm', 'd', '0', '\n', EOF};
            cli.process();

            THEN("command is not found. Console stays at T1") {
                REQUIRE(io.print_history.size() == 7);
                REQUIRE(io.print_history[0] == endline);
                REQUIRE(io.print_history[1] == "Command not found");
                REQUIRE(io.print_history[2] == endline);
                REQUIRE(io.print_history[3] == prompt);
                REQUIRE(io.print_history[4] == "[");
                REQUIRE(io.print_history[5] == "T1");
                REQUIRE(io.print_history[6] == "] ");
            }
        }

        WHEN("execute command in another table") {
            io.get_char_val = {'T', '0', ' ', 'c', 'm', 'd', '0', '\n', EOF};
            cli.process();

            THEN("command is not found. Console stays at T1") {
                REQUIRE(io.print_history.size() == 7);
                REQUIRE(io.print_history[0] == endline);
                REQUIRE(io.print_history[1] == "Command not found");
                REQUIRE(io.print_history[2] == endline);
                REQUIRE(io.print_history[3] == prompt);
                REQUIRE(io.print_history[4] == "[");
                REQUIRE(io.print_history[5] == "T1");
                REQUIRE(io.print_history[6] == "] ");
            }
        }

        WHEN("call another table") {
            io.get_char_val = {'T', '0', '\n', EOF};
            cli.process();

            THEN("command is not found. Console stays at T1") {
                // To switch to T0, user need to call quit first.
                REQUIRE(io.print_history.size() == 7);
                REQUIRE(io.print_history[0] == endline);
                REQUIRE(io.print_history[1] == "Command not found");
                REQUIRE(io.print_history[2] == endline);
                REQUIRE(io.print_history[3] == prompt);
                REQUIRE(io.print_history[4] == "[");
                REQUIRE(io.print_history[5] == "T1");
                REQUIRE(io.print_history[6] == "] ");
            }
        }

        WHEN("enter quit") {
            io.get_char_val = {'q', 'u', 'i', 't', '\n', EOF};
            cli.process();

            THEN("console returns to root") {
                REQUIRE(io.print_history.size() == 2);
                REQUIRE(io.print_history[0] == endline);
                REQUIRE(io.print_history[1] == prompt);
            }
        }

        WHEN("switch to T0 table") {
            // First quit
            io.get_char_val = {'q', 'u', 'i', 't', '\n', EOF};
            cli.process();

            // Call T0
            io.reset();
            io.get_char_val = {'T', '0', '\n', EOF};
            cli.process();

            THEN("console is at T0") {
                REQUIRE(io.print_history.size() == 5);
                REQUIRE(io.print_history[0] == endline);
                REQUIRE(io.print_history[1] == prompt);
                REQUIRE(io.print_history[2] == "[");
                REQUIRE(io.print_history[3] == "T0");
                REQUIRE(io.print_history[4] == "] ");
            }
        }

        WHEN("execute command in another table from code") {
            Cmd_result result = cli.execute("T0 cmd0");

            THEN("command is executed") {
                REQUIRE(io.print_history.size() == 2);
                REQUIRE(io.print_history[0] == endline);
                REQUIRE(io.print_history[1] == "cmd0 called");
                REQUIRE(result == Cmd_result::success);
            }

            THEN("console stays at T1") {
                io.reset();
                io.get_char_val = {'\n', EOF};
                cli.process();

                REQUIRE(io.print_history.size() == 5);
                REQUIRE(io.print_history[0] == endline);
                REQUIRE(io.print_history[1] == prompt);
                REQUIRE(io.print_history[2] == "[");
                REQUIRE(io.print_history[3] == "T1");
                REQUIRE(io.print_history[4] == "] ");
            }
        }

        WHEN("execute command in root table from code") {
            Cmd_result result = cli.execute("root_cmd0");

            THEN("command is executed") {
                REQUIRE(io.print_history.size() == 2);
                REQUIRE(io.print_history[0] == endline);
                REQUIRE(io.print_history[1] == "root_cmd0 called");
                REQUIRE(result == Cmd_result::success);
            }

            THEN("console stays at T1") {
                io.reset();
                io.get_char_val = {'\n', EOF};
                cli.process();

                REQUIRE(io.print_history.size() == 5);
                REQUIRE(io.print_history[0] == endline);
                REQUIRE(io.print_history[1] == prompt);
                REQUIRE(io.print_history[2] == "[");
                REQUIRE(io.print_history[3] == "T1");
                REQUIRE(io.print_history[4] == "] ");
            }
        }

        WHEN("T0 table is remove on runtime") {
            cli.remove_cmd_table("T0");

            THEN("T1 is still callable") {
                io.get_char_val = {'t', '1', 'c', 'm', 'd', '0', '\n', EOF};
                cli.process();

                REQUIRE(io.print_history.size() == 7);
                REQUIRE(io.print_history[0] == endline);
                REQUIRE(io.print_history[1] == "t1cmd0 called");
                REQUIRE(io.print_history[2] == endline);
                REQUIRE(io.print_history[3] == prompt);
                REQUIRE(io.print_history[4] == "[");
                REQUIRE(io.print_history[5] == "T1");
                REQUIRE(io.print_history[6] == "] ");
            }
        }
    }

    GIVEN("Console is started, echo is OFF. Multiple tables & root table are added, show active table is ON") {
        Options cli_options{
            .echo              = false,
            .show_active_table = true,
            .console_prompt    = prompt.c_str(),
            .endline           = endline.c_str(),
            .msg_error         = "Error: ",
            .msg_cmd_not_found = "Command not found",
        };

        Console cli(io, input_buffer_info, command_table_buffer_info, cli_options);
        cli.add_cmd_table("", root_cmd::get_root_table());
        cli.add_cmd_table("T0", t0_cmd::get_t0_table());
        cli.add_cmd_table("T1", t1_cmd::get_t1_table());
        cli.add_cmd_table("T2", t2_cmd::get_t2_table());
        cli.start("");
        io.reset();

        WHEN("execute command in root table") {
            io.get_char_val = {'r', 'o', 'o', 't', '_', 'c', 'm', 'd', '0', '\n', EOF};
            cli.process();

            THEN("command is called") {
                REQUIRE(io.print_history.size() == 4);
                REQUIRE(io.print_history[0] == endline);
                REQUIRE(io.print_history[1] == "root_cmd0 called");
                REQUIRE(io.print_history[2] == endline);
                REQUIRE(io.print_history[3] == prompt);
            }
        }

        WHEN("execute command in root table, followed by opening T1") {
            io.get_char_val = {'r', 'o', 'o', 't', '_', 'c', 'm', 'd', '0', '\n', EOF};
            cli.process();
            io.reset();

            io.get_char_val = {'T', '1', '\n', EOF};
            cli.process();

            THEN("T1 is opened") {
                REQUIRE(io.print_history.size() == 5);
                REQUIRE(io.print_history[0] == endline);
                REQUIRE(io.print_history[1] == prompt);
                REQUIRE(io.print_history[2] == "[");
                REQUIRE(io.print_history[3] == "T1");
                REQUIRE(io.print_history[4] == "] ");
            }
        }

        WHEN("enter help") {
            io.get_char_val = {'h', 'e', 'l', 'p', '\n', EOF};
            cli.process();

            THEN("root table commands and all table names are printed") {
                REQUIRE(io.print_history.size() == 18);
                REQUIRE(io.print_history[0] == endline);
                REQUIRE(io.print_history[1] == "");
                REQUIRE(io.print_history[2] == endline);
                REQUIRE(io.print_history[3] == "root_cmd0");
                REQUIRE(io.print_history[4] == " : ");
                REQUIRE(io.print_history[5] == "root cmd0 help");
                REQUIRE(io.print_history[6] == endline);
                REQUIRE(io.print_history[7] == "root_cmd1");
                REQUIRE(io.print_history[8] == " : ");
                REQUIRE(io.print_history[9] == "root cmd1 help");
                REQUIRE(io.print_history[10] == endline);
                REQUIRE(io.print_history[11] == "T0");
                REQUIRE(io.print_history[12] == endline);
                REQUIRE(io.print_history[13] == "T1");
                REQUIRE(io.print_history[14] == endline);
                REQUIRE(io.print_history[15] == "T2");
                REQUIRE(io.print_history[16] == endline);
                REQUIRE(io.print_history[17] == prompt);
            }
        }
    }

    GIVEN(
        "Console is started, echo is OFF. Multiple tables are added, show active table is ON, a command is executed") {
        Options cli_options{
            .echo              = false,
            .show_active_table = true,
            .console_prompt    = prompt.c_str(),
            .endline           = endline.c_str(),
            .msg_error         = "Error: ",
            .msg_cmd_not_found = "Command not found",
        };

        Console cli(io, input_buffer_info, command_table_buffer_info, cli_options);

        // To validate help command, do not place command table in alphabetical order
        cli.add_cmd_table("T2", t2_cmd::get_t2_table());
        cli.add_cmd_table("T0", t0_cmd::get_t0_table());
        cli.add_cmd_table("T1", t1_cmd::get_t1_table());

        cli.start("");
        io.reset();

        io.get_char_val = {'T', '1', ' ', ' ', 't', '1', 'c', 'm', 'd', '0', '\n', EOF};
        cli.process();
        io.reset();

        WHEN("user open table T1") {
            io.get_char_val = {'T', '1', '\n', EOF};
            cli.process();

            THEN("T1 table is opened.") {
                REQUIRE(io.print_history.size() == 5);
                REQUIRE(io.print_history[0] == endline);
                REQUIRE(io.print_history[1] == prompt);
                REQUIRE(io.print_history[2] == "[");
                REQUIRE(io.print_history[3] == "T1");
                REQUIRE(io.print_history[4] == "] ");
            }
        }
    }
}

// Test helper function implementation
void run_process(Console& cli, std::uint32_t nb_time) {
    for (std::uint32_t i = 0; i < nb_time; ++i) {
        cli.process();
    }
}
// NOLINTEND
