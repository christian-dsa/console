/**
 * Copyright 2023 Christian Daigneault-St-Arnaud
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions
 *    and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 *    following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
// NOLINTBEGIN
#ifndef MOCK_IO_H
#define MOCK_IO_H

class Mock_io : public console::Console_io {
public:
    Mock_io()
    : get_char_index(0) {
        reset();
    }

    void open() override {
        // No op
    }
    void close() override {
        // No op
    }

    void print(const char* log_message) override {
        print_history.emplace_back(log_message);
    }

    void print(const char* log_message, std::size_t size) override {
        const std::string message(log_message, size);
        print_history.emplace_back(message);
    }

    int get_char() override {
        int out;

        if (get_char_index < get_char_val.size()) {
            out = get_char_val[get_char_index];
            ++get_char_index;
        } else {
            out = get_char_val.back();
        }

        return out;
    }

    void reset() {
        print_history.clear();
        get_char_val.clear();
        get_char_index = 0;
    }

    std::uint32_t            get_char_index;
    std::vector<std::string> print_history;
    std::vector<int>         get_char_val;
};
// NOLINTEND
#endif  // MOCK_IO_H
