/**
 * Copyright 2023 Christian Daigneault-St-Arnaud
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions
 *    and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 *    following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
// NOLINTBEGIN
#include "catch2/catch.hpp"
#include "command_table_for_test.h"

using namespace console;

SCENARIO("Buffer class", "[Buffer]") {
    constexpr std::size_t                                        command_table_buffer_size = 6;
    std::array<command_table_entry_t, command_table_buffer_size> table_memory{};

    command_table_buffer_info_t command_table_buffer_info{.data = table_memory.data(), .capacity = table_memory.size()};

    Buffer<command_table_entry_t> command_table_buffer(command_table_buffer_info);

    GIVEN("Buffer is empty") {

        WHEN("multiple values are push back") {
            command_table_entry_t expected_value0{"Value3", t3_cmd::get_t3_table()};
            command_table_entry_t expected_value1{"Value1", t1_cmd::get_t1_table()};
            command_table_entry_t expected_value2{"Value2", t2_cmd::get_t2_table()};
            command_table_entry_t expected_value3{"Value0", t0_cmd::get_t0_table()};

            command_table_buffer.push_back(expected_value0);
            command_table_buffer.push_back(expected_value1);
            command_table_buffer.push_back(expected_value2);
            command_table_buffer.push_back(expected_value3);

            THEN("all values are in the buffer in given order") {
                REQUIRE(table_memory[0] == expected_value0);
                REQUIRE(table_memory[1] == expected_value1);
                REQUIRE(table_memory[2] == expected_value2);
                REQUIRE(table_memory[3] == expected_value3);
            }

            THEN("size is 4") {
                REQUIRE(command_table_buffer.size() == 4);
            }
        }
    }

    GIVEN("Buffer has some values") {
        constexpr std::size_t                            expected_size = 4;
        std::array<command_table_entry_t, expected_size> input{
            {{"Value3", t3_cmd::get_t3_table()},
             {"Value1", t1_cmd::get_t1_table()},
             {"Value2", t2_cmd::get_t2_table()},
             {"Value0", t0_cmd::get_t0_table()}}
        };

        command_table_buffer.push_back(input[0]);
        command_table_buffer.push_back(input[1]);
        command_table_buffer.push_back(input[3]);

        REQUIRE(table_memory[0] == input[0]);
        REQUIRE(table_memory[1] == input[1]);
        REQUIRE(table_memory[2] == input[3]);

        WHEN("a new value is inserted at index 1") {
            constexpr std::size_t pos_index = 1;
            auto                  pos       = command_table_buffer.begin() + pos_index;

            command_table_buffer.insert(pos, input[2]);

            THEN("new value is at index 1") {
                REQUIRE(table_memory[1] == input[2]);
            }
            THEN("size is 4") {
                REQUIRE(command_table_buffer.size() == expected_size);
            }
            THEN("all value after index 1 are moved") {
                REQUIRE(table_memory[0] == input[0]);
                REQUIRE(table_memory[1] == input[2]);
                REQUIRE(table_memory[2] == input[1]);
                REQUIRE(table_memory[3] == input[3]);
            }
        }

        WHEN("value at index 1 is erased") {
            constexpr std::size_t pos_index = 1;
            auto                  pos       = command_table_buffer.begin() + pos_index;

            command_table_buffer.erase(pos);

            THEN("all values after index 1 are moved") {
                REQUIRE(table_memory[0] == input[0]);
                REQUIRE(table_memory[1] == input[3]);
            }
        }
    }
}
// NOLINTEND
