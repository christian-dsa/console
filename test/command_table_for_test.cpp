/**
 * Copyright 2023 Christian Daigneault-St-Arnaud
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions
 *    and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 *    following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
// NOLINTBEGIN
#include "command_table_for_test.h"
#include "command_parser.h"
#include "console_util.h"

// namespace tx_cmd would be on different .cpp .h files.

using namespace console;

namespace t0_cmd {
// Application's commands functions. To keep the tests simple, all commands only print its name.
static Cmd_result cmd0(Console_io* io, const std::string_view& buffer) {
    io->print("\r\n");
    io->print("cmd0 called");
    return Cmd_result::success;
}
static Cmd_result cmd1(Console_io* io, const std::string_view& buffer) {
    io->print("\r\n");
    io->print("cmd1 called");
    return Cmd_result::success;
}
static Cmd_result cmd2(Console_io* io, const std::string_view& buffer) {
    io->print("\r\n");
    io->print("cmd2 called");
    return Cmd_result::success;
}
static Cmd_result cmd3(Console_io* io, const std::string_view& buffer) {
    io->print("\r\n");
    io->print("cmd3 called");
    return Cmd_result::success;
}
static Cmd_result cmd4(Console_io* io, const std::string_view& buffer) {
    io->print("\r\n");
    io->print("cmd4 called");
    return Cmd_result::error;
}

// Application's commands table
constexpr bool           option     = false;
static constexpr Command t0_table[] = {
    {   "cmd0",   &cmd0, help<option>("cmd0 help")},
    {   "cmd1",   &cmd1, help<option>("cmd1 help")},
    {   "cmd2",   &cmd2, help<option>("cmd2 help")},
    {   "cmd3",   &cmd3, help<option>("cmd3 help")},
    {   "cmd4",   &cmd4, help<option>("cmd4 help")},
    {table_end, nullptr,                   nullptr}
};

const Command* get_t0_table() {
    return t0_table;
}
}  // namespace t0_cmd

namespace t1_cmd {

static Cmd_result t1cmd0(Console_io* io, const std::string_view& buffer) {
    io->print("\r\n");
    io->print("t1cmd0 called");
    return Cmd_result::success;
}
static Cmd_result t1cmd1(Console_io* io, const std::string_view& buffer) {
    io->print("\r\n");
    io->print("t1cmd1 called");
    return Cmd_result::error;
}
static Cmd_result t11(Console_io* io, const std::string_view& buffer) {
    io->print("\r\n");
    io->print("t1 1 called");
    return Cmd_result::success;
}

constexpr bool           option     = true;
static constexpr Command t1_table[] = {
    { "t1cmd0", &t1cmd0, help<option>("t1cmd0 help")},
    { "t1cmd1", &t1cmd1, help<option>("t1cmd1 help")},
    {      "1",    &t11,      help<option>("1 help")},
    {table_end, nullptr,                     nullptr}
};

const Command* get_t1_table() {
    return t1_table;
}
}  // namespace t1_cmd

namespace t2_cmd {

static Cmd_result t2cmd0(Console_io* io, const std::string_view& buffer) {
    io->print("\r\n");
    io->print("t2cmd0 called");
    return Cmd_result::success;
}
static Cmd_result t2cmd1(Console_io* io, const std::string_view& buffer) {
    io->print("\r\n");
    io->print("t2cmd1 called");
    return Cmd_result::success;
}

constexpr bool           option     = true;
static constexpr Command t2_table[] = {
    { "t2cmd0", &t2cmd0, help<option>("t2cmd0 help")},
    { "t2cmd1", &t2cmd1, help<option>("t2cmd1 help")},
    {table_end, nullptr,                     nullptr}
};

const Command* get_t2_table() {
    return t2_table;
}
}  // namespace t2_cmd

namespace t3_cmd {

static Cmd_result t3cmd0(Console_io* io, const std::string_view& buffer) {
    io->print("\r\n");
    io->print("t3cmd0 called");
    return Cmd_result::success;
}
static Cmd_result t3cmd1(Console_io* io, const std::string_view& buffer) {
    io->print("\r\n");
    io->print("t3cmd1 called");
    return Cmd_result::success;
}

constexpr bool           option     = true;
static constexpr Command t3_table[] = {
    { "t3cmd0", &t3cmd0, help<option>("t3cmd0 help")},
    { "t3cmd1", &t3cmd1, help<option>("t3cmd1 help")},
    {table_end, nullptr,                     nullptr}
};

const Command* get_t3_table() {
    return t3_table;
}
}  // namespace t3_cmd

namespace root_cmd {
static Cmd_result root_cmd0(Console_io* io, const std::string_view& buffer) {
    io->print("\r\n");
    io->print("root_cmd0 called");
    return Cmd_result::success;
}
static Cmd_result root_cmd1(Console_io* io, const std::string_view& buffer) {
    io->print("\r\n");
    io->print("root_cmd1 called");
    return Cmd_result::success;
}

constexpr bool           option       = true;
static constexpr Command root_table[] = {
    {"root_cmd0", &root_cmd0, help<option>("root cmd0 help")},
    {"root_cmd1", &root_cmd1, help<option>("root cmd1 help")},
    {  table_end,    nullptr,                        nullptr}
};

const Command* get_root_table() {
    return root_table;
}
}  // namespace root_cmd
// NOLINTEND
