/**
 * Copyright 2023 Christian Daigneault-St-Arnaud
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions
 *    and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 *    following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef CONSOLE_UTIL_H
#define CONSOLE_UTIL_H

// Includes
#include "console_type.h"
#include <charconv>
#include <cstdint>
#include <string_view>
#include <type_traits>

namespace console {

// Enable / Disable help message in table
template<bool enable>
constexpr const char* help(const char* help_message) {
    if (enable) {
        return help_message;
    }
    return "";
}

namespace conversion {

enum class Error { ok, out_of_range, invalid_input, destination_too_small };

enum class Format : int { bin = 2, dec = 10, hex = 16 };

// Convert string to integral type. Detect the value representation (hex, dec or bin).
template<typename type>
type str_to_integral(const std::string_view& str, Error& error) {
    static_assert(std::is_integral<type>());
    constexpr int base_hex = 16;
    constexpr int base_dec = 10;
    constexpr int base_bin = 2;

    type output;

    error    = Error::ok;
    int base = base_dec;

    const auto* p_begin = str.begin();

    // Detect prefix
    if (!(str.compare(0, 2, "0x"))) {
        p_begin += 2;
        base    = base_hex;
    } else if (!(str.compare(0, 2, "0b"))) {
        p_begin += 2;
        base    = base_bin;
    }

    // Conversion
    auto [ptr, ec]{std::from_chars(p_begin, str.data() + str.size(), output, base)};

    // Error handling
    if ((ec == std::errc::invalid_argument) || (ptr != str.end())) {
        error  = Error::invalid_input;
        output = 0;
    } else if (ec == std::errc::result_out_of_range) {
        error  = Error::out_of_range;
        output = 0;
    }

    return output;
}

// Convert integral type to string. Support value representation hex, dec or bin.
// A NULL char is placed at the end.
// Return number of char used in buffer. Doesn't include the NULL char.
template<typename type>
std::size_t integral_to_str(type value, char* begin, char* end, Format fmt, Error& error) {
    static_assert(std::is_integral<type>());
    error    = Error::ok;
    char* it = begin;

    // Append prefix if needed
    const char* prefix     = "";
    const char* hex_prefix = "0x";
    const char* bin_prefix = "0b";

    if (fmt == Format::hex) {
        prefix = hex_prefix;
    } else if (fmt == Format::bin) {
        prefix = bin_prefix;
    }

    for (; (it != end) && (*prefix != '\0'); ++it) {
        *it = *prefix;
        ++prefix;
    }

    // Conversion
    int base = static_cast<int>(fmt);
    auto [ptr, ec]{std::to_chars(it, end, value, base)};

    if (ptr != end) {
        *ptr = '\0';
    }

    // Error handling
    if (ec == std::errc::value_too_large) {
        error  = Error::destination_too_small;
        ptr    = begin;
        *begin = '\0';
    }

    return ptr - begin;
}
}  // namespace conversion
}  // namespace console

#endif /* CONSOLE_UTIL_H */
