/**
 * Copyright 2023 Christian Daigneault-St-Arnaud
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions
 *    and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 *    following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef CONSOLE_H
#define CONSOLE_H

#include "console_io.h"
#include "console_type.h"
#include "console_util.h"
#include <array>
#include <string_view>

namespace console {
class Console {
public:
    Console(Console_io&                  console_io,
            input_buffer_info_t&         input_buffer,
            command_table_buffer_info_t& table_buffer_info,
            Options&                     console_options)
    : console_io(&console_io),
      current_table_name(table_end),
      input_buffer(input_buffer),
      table_buffer(table_buffer_info),
      console_options(&console_options) {}

    void start(const char* message = "");

    void process();

    // Some commands are high level / global and don't belong to a specific table.
    // To accommodate this case, all global commands must be in the same command table named "". They can
    // only be called when no table is active.
    void add_cmd_table(const char* name, const Command* table);

    void remove_cmd_table(const char* name);

    void set_interface(Console_io& io);

    Cmd_result execute(const char* command);

    void set_options(Options& options);

private:
    bool get_input();

    void execute_help();

    void open_table(Buffer<command_table_entry_t>::iterator table);

    void reset_table();

    bool is_table_found(Buffer<command_table_entry_t>::iterator table);

    void update_table(Buffer<command_table_entry_t>::iterator table_it);

    void set_eol_flag(int ch);

    void reset_eol_flag();

    [[nodiscard]] bool is_eol_crlf_char() const;

    void execute_built_in_command(int index);

    Console_io*                   console_io;
    const Command*                current_table{nullptr};
    const char*                   current_table_name;
    Buffer<char>                  input_buffer;
    Buffer<command_table_entry_t> table_buffer;
    const Options*                console_options;
    bool                          cr_eol_flag{false};
    bool                          lf_eol_flag{false};
    bool                          key_sequence{false};
};
}  // namespace console

#endif /* CONSOLE_H */
