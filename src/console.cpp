/**
 * Copyright 2023 Christian Daigneault-St-Arnaud
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions
 *    and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 *    following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "console.h"
#include "command_parser.h"
#include "console_type.h"
#include <algorithm>
#include <cstdio>
#include <cstring>

namespace console {
// Private functions
static bool is_command_found(std::int32_t index) {
    return (index > not_found);
}

static void remove_table_from_input(std::string_view& input, const std::string_view& name) {
    std::size_t nb_separator = 0;
    std::size_t len          = name.size();

    for (const auto* it = input.begin() + len; it != input.end(); ++it) {
        if (*it == separator) {
            ++nb_separator;
        } else {
            break;
        }
    }
    len += nb_separator;
    input.remove_prefix(len);
}

static bool is_backspace(int ch) {
    return ((ch == bs_char) || (ch == del_char));
}

static bool is_end_of_line(int ch) {
    return ((ch == cr_char) || (ch == lf_char));
}

static void fill_input_buffer(Buffer<char>& input_buffer, int ch) {
    if (input_buffer.size() < input_buffer.capacity()) {
        input_buffer.push_back(static_cast<char>(ch));
    } else if (is_end_of_line(ch)) {
        input_buffer.back() = static_cast<char>(ch);
    }
}

static void delete_char(Buffer<char>& input_buffer, Console_io* io, bool echo) {
    constexpr const char* delete_echo = "\b \b";

    if (!input_buffer.empty()) {
        input_buffer.pop_back();

        if (echo) {
            io->print(delete_echo);
        }
    }
}

static void print_prefix_and_message(Console_io*    io,
                                     const Options* options,
                                     const char*    console_prefix,
                                     const char*    message) {
    io->print(options->endline);
    io->print(console_prefix);
    io->print(message);
}

static void print_message(Console_io* io, const Options* options, const char* message) {
    io->print(options->endline);
    io->print(message);
}

static bool is_table_open(const Command* current_table) {
    return (current_table != nullptr);
}

static bool is_table_root(const char* current_table_name) {
    return (std::strcmp(current_table_name, root_table_name) == 0);
}

static bool is_table_end(const char* command_name) {
    return std::strcmp(command_name, table_end) == 0;
}

static std::size_t get_max_name_length(const Command* current_table) {
    std::uint32_t max_length = 0;
    std::int32_t  index      = 0;
    while (!is_table_end(current_table[index].name)) {
        const std::uint32_t name_length = std::strlen(current_table[index].name);
        max_length                      = std::max(max_length, name_length);
        ++index;
    }

    return max_length;
}

static void print_table_help(Console_io*    io,
                             const Options* options,
                             const Command* current_table,
                             const char*    current_table_name) {
    const char* separation = " : ";
    const char* space      = " ";

    const std::size_t max_length = get_max_name_length(current_table);

    io->print(options->endline);
    io->print(current_table_name);

    std::int32_t index = 0;
    while (!is_table_end(current_table[index].name)) {
        const std::size_t nb_space = max_length - std::strlen(current_table[index].name);

        io->print(options->endline);
        io->print(current_table[index].name);

        for (std::size_t j = 0; j < nb_space; ++j) {
            io->print(space);
        }
        io->print(separation);
        io->print(current_table[index].help);
        ++index;
    }
}

static void start_new_line(Console_io*    io,
                           const Options* options,
                           const char*    current_table_name,
                           Buffer<char>&  input_buffer) {
    io->print(options->endline);
    io->print(options->console_prompt);

    if ((*current_table_name != null_char) && options->show_active_table) {
        io->print("[");
        io->print(current_table_name);
        io->print("] ");
    }

    input_buffer.clear();
}

static int find_command(const std::string_view& name, const Command* current_table) {
    int index = 0;
    if (is_table_open(current_table)) {
        while (!is_table_end(current_table[index].name)) {
            if (name.compare(current_table[index].name) == 0) {
                return index;
            }
            ++index;
        }
    }
    return not_found;
}

static int find_built_in_command(const std::string_view& name) {
    int index = not_found;

    if (name.compare(quit_name) == 0) {
        index = quit_index;
    } else if (name.compare(help_name) == 0) {
        index = help_index;
    }
    return index;
}

static Buffer<command_table_entry_t>::iterator find_table(const std::string_view         name,
                                                          Buffer<command_table_entry_t>& table_buffer) {
    auto* it = std::find_if(table_buffer.begin(), table_buffer.end(), [&](command_table_entry_t& entry) {
        return name.compare(entry.first) == 0;
    });
    return it;
}

static bool is_key_sequence(int ch, bool& key_sequence) {
    bool is_sequence = true;

    if (!key_sequence) {
        is_sequence = false;

        if (ch == vt100_esc) {
            key_sequence = true;
            is_sequence  = true;
        }
    } else {
        switch (ch) {
        case vt100_cursor_up:
        case vt100_cursor_down:
        case vt100_cursor_right:
        case vt100_cursor_left:
        case vt100_p1_key:
            key_sequence = false;
        default:
            break;
        }
    }
    return is_sequence;
}

// Private methods
inline void Console::open_table(Buffer<command_table_entry_t>::iterator table) {
    current_table_name = table->first;
    current_table      = table->second;
}

inline void Console::reset_table() {
    current_table_name = root_table_name;
    current_table      = nullptr;
}

inline bool Console::is_table_found(Buffer<command_table_entry_t>::iterator table) {
    return (table != table_buffer.end());
}

inline void Console::update_table(Buffer<command_table_entry_t>::iterator table_it) {
    if (is_table_found(table_it)) {
        open_table(table_it);
    } else {
        reset_table();
    }
}

inline void Console::set_eol_flag(int ch) {
    if (ch == cr_char) {
        cr_eol_flag = true;
    } else if (ch == lf_char) {
        lf_eol_flag = true;
    }
}
inline void Console::reset_eol_flag() {
    cr_eol_flag = false;
    lf_eol_flag = false;
}

inline bool Console::is_eol_crlf_char() const {
    return (cr_eol_flag && lf_eol_flag);
}

inline void Console::execute_built_in_command(int index) {
    switch (index) {
    case quit_index:
        reset_table();
        break;
    case help_index:
        execute_help();
        break;
    default:
        break;
    }
}

// Public methods
void Console::start(const char* message) {
    if (table_buffer.size() == single_table) {
        open_table(table_buffer.begin());
    }
    print_message(console_io, console_options, message);
    start_new_line(console_io, console_options, current_table_name, input_buffer);
}

void Console::process() {
    bool reset_active_table = false;

    // Get user input. Exit if command is not complete.
    if (!get_input()) {
        return;
    }

    // Process command
    std::string_view input{input_buffer.data(), input_buffer.size()};
    parser::Parser   parser(input);
    std::string_view name = parser.get_name();

    // Table?

    const std::array<std::string_view, table_search_size> table_search{name, root_table_name};

    if (!is_table_open(current_table) || is_table_root(current_table_name)) {
        for (const auto& table_name : table_search) {
            Buffer<command_table_entry_t>::iterator table = find_table(table_name, table_buffer);
            if (is_table_found(table)) {
                open_table(table);

                remove_table_from_input(input, table_name);
                name = parser.get_name();

                if (!name.empty()) {
                    reset_active_table = true;
                }
                break;
            }
        }
    }

    // Command?
    if (!name.empty()) {
        int index = find_command(name, current_table);

        if (is_command_found(index)) {
            const Cmd_result result = current_table[index].execute(console_io, input);

            if (result != Cmd_result::success) {
                print_prefix_and_message(console_io,
                                         console_options,
                                         console_options->msg_error,
                                         current_table[index].help);
            }
        } else {
            index = find_built_in_command(name);

            if (is_command_found(index)) {
                execute_built_in_command(index);
            } else {
                // Not found
                print_message(console_io, console_options, console_options->msg_cmd_not_found);
            }
        }
    }

    // reset active table?
    if (reset_active_table) {
        reset_table();
    }

    // Prepare new line
    start_new_line(console_io, console_options, current_table_name, input_buffer);
}

void Console::add_cmd_table(const char* name, const Command* table) {
    if (table_buffer.size() < table_buffer.capacity()) {
        auto* pos = std::find_if(table_buffer.begin(), table_buffer.end(), [&](command_table_entry_t& entry) {
            return std::strcmp(name, entry.first) < 0;
        });
        table_buffer.insert(pos, {name, table});

        if (is_table_open(current_table)) {  // Iterators are not good anymore, need to update
            auto* table_it = find_table(current_table_name, table_buffer);
            update_table(table_it);
        }
    }
}

void Console::remove_cmd_table(const char* name) {
    auto* it = find_table(name, table_buffer);

    if (is_table_found(it)) {
        table_buffer.erase(it);

        if (is_table_open(current_table)) {  // Iterators are not good anymore, need to update
            auto* table_it = find_table(current_table_name, table_buffer);
            update_table(table_it);
        }
    }
}

void Console::set_interface(Console_io& io) {
    console_io = &io;
}

Cmd_result Console::execute(const char* command) {
    Cmd_result result = Cmd_result::error;

    std::string_view input{command};
    parser::Parser   parser(input);
    const auto*      temp_current_table_name = current_table_name;
    const auto*      temp_current_table      = current_table;

    // Table?
    std::string_view name     = parser.get_name();
    auto*            table_it = find_table(name, table_buffer);
    if (is_table_found(table_it)) {
        open_table(table_it);
        remove_table_from_input(input, name);
        name = parser.get_name();  // get command name
    } else {
        // No table given, check within root's table (if it exists)
        table_it = find_table(root_table_name, table_buffer);
        if (is_table_found(table_it)) {
            open_table(table_it);
        }
    }

    // Command?
    const int index = find_command(name, current_table);

    if (is_command_found(index)) {
        result = current_table[index].execute(console_io, input);
    }

    // Go back to original active table
    current_table_name = temp_current_table_name;
    current_table      = temp_current_table;

    return result;
}

void Console::set_options(Options& options) {
    console_options = &options;
}

// Private methods
bool Console::get_input() {
    static constexpr std::size_t    echo_out_size = 2;
    std::array<char, echo_out_size> echo_out      = {' ', '\0'};
    bool                            is_eol_found  = false;

    int ch = 0;
    while ((ch != EOF) && (!is_eol_found)) {
        ch = console_io->get_char();

        if (ch != EOF) {
            if (is_backspace(ch)) {
                delete_char(input_buffer, console_io, console_options->echo);
            } else if (is_end_of_line(ch)) {
                set_eol_flag(ch);
                if (!is_eol_crlf_char()) {
                    is_eol_found = true;
                } else {
                    reset_eol_flag();
                    ch = EOF;
                }
            } else if (!is_key_sequence(ch, key_sequence)) {
                fill_input_buffer(input_buffer, ch);
                if (console_options->echo) {
                    echo_out[0] = static_cast<char>(ch);
                    console_io->print(echo_out.data());
                }
            }
        }
    }

    return is_eol_found;
}

void Console::execute_help() {

    if (is_table_root(current_table_name)) {
        // If root table has commands, print all available commands
        auto* table = find_table(root_table_name, table_buffer);
        if (is_table_found(table)) {
            print_table_help(console_io, console_options, table->second, root_table_name);
        }

        // Print all table name
        for (auto& it : table_buffer) {
            if (strlen(it.first) > 0) {
                console_io->print(console_options->endline);
                console_io->print(it.first);
            }
        }
    } else {
        print_table_help(console_io, console_options, current_table, current_table_name);
    }
}
}  // namespace console
