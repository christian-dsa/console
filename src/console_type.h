/**
 * Copyright 2023 Christian Daigneault-St-Arnaud
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions
 *    and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 *    following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef CONSOLE_TYPE_H
#define CONSOLE_TYPE_H

// Includes
#include "console_io.h"
#include <algorithm>
#include <string_view>

namespace console {

// Command
enum class Cmd_result { error, success };

using command_t = Cmd_result (*)(Console_io* io, const std::string_view& input);

struct Command {
    const char*        name;
    console::command_t execute;
    const char*        help;
};

// Options
struct Options {
    bool        echo              = true;                 // Print each received character.
    bool        show_active_table = true;                 // Show active table next to prompt. Ex: > [table_name].
    const char* console_prompt    = "> ";                 // Printed at the beginning of each line.
    const char* endline           = "\r\n";               // Printed at the end of each line.
    const char* msg_error         = "Error: ";            // Printed when a command returns error status followed by
                                                          // its help messages.
    const char* msg_cmd_not_found = "Command not found";  // Printed when a command is not found.
};

// Buffer type
template<typename type>
struct Buffer_info {
    type*       data;
    std::size_t capacity;
};

template<typename type>
class Buffer {
public:
    explicit Buffer(Buffer_info<type>& buffer_info)
    : info(&buffer_info) {}

    using iterator = type*;

    iterator begin() {
        return info->data;
    }

    iterator end() {
        return &info->data[buffer_size];
    }

    type* data() {
        return info->data;
    }

    [[nodiscard]] std::size_t size() const {
        return buffer_size;
    }

    void clear() {
        buffer_size = 0;
    }

    [[nodiscard]] bool empty() const {
        return buffer_size == 0;
    }

    type& back() {
        // The behavior is undefined if empty() == true
        return info->data[buffer_size - 1];
    }

    void push_back(type value) {
        info->data[buffer_size] = value;
        ++buffer_size;
    }

    void insert(iterator pos, const type& value) {
        std::copy_backward(pos, end(), end() + 1);
        *pos = value;
        ++buffer_size;
    }

    void erase(iterator pos) {
        std::copy(pos + 1, end(), pos);
        --buffer_size;
    }

    void pop_back() {
        --buffer_size;
    }

    [[nodiscard]] std::size_t capacity() const {
        return info->capacity;
    }

private:
    Buffer_info<type>* info;
    std::size_t        buffer_size{0};
};

// Input buffer
using input_buffer_info_t = Buffer_info<char>;

// Command table buffer
using command_table_entry_t       = std::pair<const char*, const Command*>;
using command_table_buffer_info_t = Buffer_info<command_table_entry_t>;

// Constants
static constexpr char        cr_char           = '\n';
static constexpr char        lf_char           = '\r';
static constexpr char        null_char         = '\0';
static constexpr char        bs_char           = 8;
static constexpr char        del_char          = 127;
static constexpr const char* root_table_name   = "";
static constexpr const char* table_end         = "";
static constexpr const char* quit_name         = "quit";
static constexpr const char* help_name         = "help";
static constexpr const int   not_found         = -1;
static constexpr const int   quit_index        = 0;
static constexpr const int   help_index        = 1;
static constexpr const char  separator         = ' ';
static constexpr const int   single_table      = 1;
static constexpr const int   table_search_size = 2;

// VT100
static constexpr char vt100_esc          = 0x1B;
static constexpr char vt100_cursor_up    = 0x41;
static constexpr char vt100_cursor_down  = 0x42;
static constexpr char vt100_cursor_right = 0x43;
static constexpr char vt100_cursor_left  = 0x44;
static constexpr char vt100_p1_key       = 0x7E;
}  // namespace console

#endif /* CONSOLE_TYPE_H */
