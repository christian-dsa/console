/**
 * Copyright 2023 Christian Daigneault-St-Arnaud
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions
 *    and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 *    following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef CONSOLE_IO_H
#define CONSOLE_IO_H

#include <cstddef>

namespace console {

class Console_io {
public:
    // Open and close I/O.
    virtual void open()  = 0;
    virtual void close() = 0;

    // Print string.
    virtual void print(const char* out)                   = 0;
    virtual void print(const char* out, std::size_t size) = 0;

    // Get first available character. Return EOF (-1) if no key is pressed.
    virtual int get_char() = 0;

    // Destructor
    virtual ~Console_io() = default;

    Console_io()                             = default;  // Default constructor
    Console_io(const Console_io&)            = delete;   // Non construction-copyable
    Console_io& operator=(const Console_io&) = delete;   // non copyable
    Console_io(Console_io&&)                 = delete;   // non movable
    Console_io& operator=(Console_io&&)      = delete;   // No move assignment operator
};
}  // namespace console

#endif /* CONSOLE_IO_H */
