/**
 * Copyright 2022 Christian Daigneault-St-Arnaud
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions
 *    and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 *    following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

// Includes
#include "command_parser.h"
#include "console_type.h"
#include <array>

namespace console::parser {
// Private functions
static bool is_argument_found(std::size_t p_begin) {
    return p_begin != std::string_view::npos;
}

static std::size_t get_nb_consecutive_char(const std::string_view& input, char character, std::size_t p_begin) {
    std::size_t index = p_begin;
    while ((input[index] == character) && (&input[index] < input.end())) {
        ++index;
    }
    return index - p_begin;
}

static std::size_t find_argument_begin(const std::string_view& input, std::uint8_t param_number, std::size_t p_begin) {
    std::uint8_t parameter_index = 0;
    std::size_t  parameter_begin = p_begin;
    std::size_t  search_begin    = p_begin;

    while ((parameter_begin != std::string_view::npos) && (parameter_index < param_number)) {
        parameter_begin = input.find(separator, search_begin);

        if (is_argument_found(parameter_begin)) {
            const std::size_t nb_ch = get_nb_consecutive_char(input, separator, parameter_begin);
            parameter_begin         += nb_ch;

            ++parameter_index;
            search_begin = parameter_begin;
        }
    }
    return parameter_begin;
}

static std::size_t find_argument_end(const std::string_view& input, std::size_t p_begin) {
    static constexpr std::size_t                    str_end_size = 4;
    static constexpr std::array<char, str_end_size> str_end      = {cr_char, lf_char, separator, null_char};

    const std::size_t first_char = p_begin + 1;

    std::size_t p_end = input.find_first_of(str_end.data(), first_char);

    if (p_end == std::string_view::npos) {
        // Reached end of string
        p_end = input.size();
    }

    return p_end;
}

// Public methods
std::string_view Parser::get_name() {
    constexpr std::size_t name_parameter = 0;
    return get_argument(name_parameter);
}

std::string_view Parser::get_argument(std::uint8_t arg_number) {
    const std::string_view output(*input);
    std::size_t            p_begin = 0;

    p_begin                 = get_nb_consecutive_char(*input, separator, p_begin);
    p_begin                 = find_argument_begin(*input, arg_number, p_begin);
    const std::size_t p_end = find_argument_end(*input, p_begin);

    if (is_argument_found(p_begin)) {
        return output.substr(p_begin, p_end - p_begin);
    }
    return "";
}
}  // namespace console::parser
