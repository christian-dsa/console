# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## 2.0.3 - 2023-03-18

### Added
- Build step with GCC and Clang.
- Cover demo code in clang-format check.

### Fixed
- Changelog version number.
- Removed commented code in .gitlab-ci.yml.
- Coding style in demo code.

### Removed
- Unused function "is_command_complete".

### Changed
- Clang image version in ci/cd.

## 2.0.2 - 2023-03-12

### Added
- Static analysis pipeline stage.
- Unit test pipeline stage using multiple configuration.

### Changed
- Refactoring to respect coding standard and rules.

## 2.0.1 - 2022-10-05

### Added
- BSD license on all files.

### Fixed
- Use static and volatile keyword in example.
- Disable new linker warning (--no-warn-rwx-segments).

## 2.0.0 - 2022-05-07

### Added
- Method to change the interface in runtime.
- Create library.
- Any command table can be the "Root" table, simply set its name to "".
- Command can be executed from code.
- Can execute a command in a specific table when no table is currently active.
- Show commands in root table (if any) when using "help" command.
- Ignore invalid key like arrows, page up/down etc.
- Namespace console for the whole library.
- Add function to convert string to integral.
- Add function to convert integral to string.
- Add open / close in IO interface.
- Demo using STM32F446RE Nucleo board.

### Changed
- Input buffer type is now an array of char instead of a std::pmr::string.
- Moved all console_options from constructor to option structure.
- Parser now use string_view instead of pmr::string.
- Split test in multiples files.

### Removed
- Internal buffer to process commands.
- std::pmr usage.
- string factory to built string on stack.

## 1.2.0 - 2020-10-24

### Added
- Unit test for input out_of_range.
- Handle input out_of_range.

### Changed
- Root table is now using a custom type. Much smaller memory footprint.

### Fixed
- std::vector type in unit test's mock.
- Moved project in CMakeLists to UNIT_TESTS section.
- Use new search function to avoid parsing twice the command's name.
- Unit test testing echo disabled.

### Removed
- Fill string input_buffer with NULL character in constructor.

## 1.1.0 - 2020-10-4
### Added
- New class with console's data.
- BSD-3 license.
- add_cmd_table & remove_cmd_table methods.

### Changed
- Remove dynamic allocation.
- Replaced makefile by CMakeList.
- String is passed by reference to commands.

### Removed
- Concept of Master table and Base table.

## 1.0.0 - 2020-01-19

Initial release
