/**
 * Copyright 2022 Christian Daigneault-St-Arnaud
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions
 *    and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 *    following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "console.h"
#include "stm32f4xx.h"

// LED
static GPIO_TypeDef*           led_port = GPIOA;
static constexpr std::uint32_t led_pin  = 5;

// BUTTON
static GPIO_TypeDef*           btn_port = GPIOC;
static constexpr std::uint32_t btn_pin  = 13;

// Console
static constexpr std::size_t                   cli_input_buffer_size = 128;
static std::array<char, cli_input_buffer_size> cli_input_buffer;
static console::input_buffer_info_t            cli_input_buffer_info{.data     = cli_input_buffer.data(),
                                                                     .capacity = cli_input_buffer.max_size()};

static constexpr std::size_t                                             cli_table_buffer_size = 8;
static std::array<console::command_table_entry_t, cli_table_buffer_size> cli_table_buffer;
static console::command_table_buffer_info_t cli_table_buffer_info{.data     = cli_table_buffer.data(),
                                                                  .capacity = cli_table_buffer.max_size()};

static console::Options cli_options{.echo              = true,
                                    .show_active_table = true,
                                    .console_prompt    = "> ",
                                    .endline           = "\r\n",
                                    .msg_error         = "Error: ",
                                    .msg_cmd_not_found = "Command not found"};

// Console io
console::Console_io& get_console_io_impl();  // Defined in io_vcp.cpp

// Board
static void board_init() {
    // Clock init
    RCC->AHB1ENR |= 0x000000FF;  // Enable ALL gpio port clock

    // LED init
    led_port->MODER   |= 0x400;      // Set PA5 to Output mode.
    led_port->OSPEEDR |= 0xC00;      // Set both PA2 High speed.
    led_port->BSRR    |= (1 << 21);  // Turn led OFF

    // BUTTON init
    // Nothing to do.
}

static void led_on() {
    led_port->BSRR |= (1 << 5);
}

static void led_off() {
    led_port->BSRR |= (1 << 21);
}

static bool is_btn_pressed() {
    if (!(btn_port->IDR & (1 << btn_pin))) {
        return true;  // BTN Pressed
    }
    return false;  // BTN Not pressed
}

// Application commands

// LED commands
static console::Cmd_result on(console::Console_io* io, const std::string_view& buffer) {
    (void)io;
    (void)buffer;
    led_on();
    return console::Cmd_result::success;
}

static console::Cmd_result off(console::Console_io* io, const std::string_view& buffer) {
    (void)io;
    (void)buffer;
    led_off();
    return console::Cmd_result::success;
}

static constexpr bool                                         led_commands_help = true;
static constexpr std::size_t                                  led_table_size    = 3;
static constexpr std::array<console::Command, led_table_size> led_commands{
    {{"on", &on, console::help<led_commands_help>("Turn on user led.")},
     {"off", &off, console::help<led_commands_help>("Turn off user led.")},
     {console::table_end, nullptr, nullptr}}
};

// Button commands
static console::Cmd_result read(console::Console_io* io, const std::string_view& buffer) {
    (void)buffer;

    if (is_btn_pressed()) {
        io->print("\r\nButton is pressed.");
    } else {
        io->print("\r\nButton is idle.");
    }
    return console::Cmd_result::success;
}

static constexpr bool                                         btn_commands_help = true;
static constexpr std::size_t                                  btn_table_size    = 2;
static constexpr std::array<console::Command, btn_table_size> btn_commands{
    {{"read", &read, console::help<btn_commands_help>("Return user button state.")},
     {console::table_end, nullptr, nullptr}}
};

int main() {

    // Init user led and button.
    board_init();

    // Console ctor
    console::Console_io& cli_io = get_console_io_impl();
    console::Console     cli(cli_io, cli_input_buffer_info, cli_table_buffer_info, cli_options);

    // Add command
    cli.add_cmd_table("led", led_commands.data());
    cli.add_cmd_table("btn", btn_commands.data());

    // Open the console io and start the console.
    cli_io.open();
    cli.start("Welcome to console demo!");

    // Process console.
    while (true) {
        cli.process();
    }
}
