
# Executable
set(EXECUTABLE ${PROJECT_NAME}_demo.elf)
add_executable(${EXECUTABLE} "")

# Compiler flags and features
include(example/nucleo-stm32f446re/stm32f4.cmake)
target_compile_features(${EXECUTABLE} PRIVATE cxx_std_17)
target_compile_features(${EXECUTABLE} PRIVATE c_std_11)
set_target_properties(${EXECUTABLE} PROPERTIES LINKER_LANGUAGE CXX)


# Sources, includes and defines
set(DEMO_SOURCES
    ${CMAKE_CURRENT_LIST_DIR}/cmsis/system_stm32f4xx.c
    ${CMAKE_CURRENT_LIST_DIR}/stm32f446re_startup.cpp
    ${CMAKE_CURRENT_LIST_DIR}/main.cpp
    ${CMAKE_CURRENT_LIST_DIR}/io_vcp.cpp)

set(DEMO_INCLUDES
    ${CMAKE_CURRENT_LIST_DIR}/cmsis
    ${CMAKE_CURRENT_LIST_DIR}
    )
set(DEMO_DEFINES STM32F446xx)
target_sources(${EXECUTABLE} PRIVATE ${DEMO_SOURCES})
target_include_directories(${EXECUTABLE} PRIVATE ${DEMO_INCLUDES})
target_compile_definitions(${EXECUTABLE} PUBLIC ${DEMO_DEFINES})
target_link_libraries(${EXECUTABLE} PRIVATE console)

# Linker
set(LINKER_SCRIPT ${CMAKE_CURRENT_LIST_DIR}/stm32f446re_linker.ld)
target_link_options(${EXECUTABLE} PRIVATE
    -T ${LINKER_SCRIPT}
    -Wl,-Map=${PROJECT_NAME}.map
    )

# Print executable size
add_custom_command(TARGET ${EXECUTABLE}
    POST_BUILD
    COMMAND arm-none-eabi-size ${EXECUTABLE}
    )

# Create hex file
add_custom_command(TARGET ${EXECUTABLE}
    POST_BUILD
    COMMAND arm-none-eabi-objcopy -O ihex ${EXECUTABLE} ${PROJECT_NAME}.hex
    COMMAND arm-none-eabi-objcopy -O binary ${EXECUTABLE} ${PROJECT_NAME}.bin
    )
