/**
 * Copyright 2022 Christian Daigneault-St-Arnaud
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions
 *    and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 *    following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "circular_buffer.h"
#include "console_io.h"
#include "stm32f4xx.h"
#include <cstdint>

// Peripherals (GPIO & UART)
static GPIO_TypeDef*           uart_gpio_port = GPIOA;
static constexpr std::uint32_t uart_tx_pin    = 2;
static constexpr std::uint32_t uart_rx_pin    = 3;
static constexpr std::uint32_t uart_gpio_af   = 7;

static USART_TypeDef*          uart_instance = USART2;
static constexpr std::uint32_t baud_rate     = 115200;

static constexpr std::size_t                      buffer_size = 128;
static circular_buffer<std::uint8_t, buffer_size> buffer;

class IO_vcp : public console::Console_io {
public:
    IO_vcp() = default;

    void open() override {
        // GPIO init
        uart_gpio_port->MODER   |= 0xA0;    // Set both PA2 & PA3 to Alternate function mode.
        uart_gpio_port->OSPEEDR |= 0x30;    // Set both PA2 High speed.
        uart_gpio_port->PUPDR   |= 0x40;    // Enable pull up on PA3.
        uart_gpio_port->AFR[0]  |= 0x7700;  // Set PA2 & PA3 to alternate function 7.

        // UART init (115200 8N1)
        RCC->APB1ENR       |= RCC_APB1ENR_USART2EN;
        uart_instance->BRR = 0x8B;    // System core clock = 16MHz.
        uart_instance->CR3 = 0x01;    // Enable Error interrupt.
        uart_instance->CR1 = 0x202C;  // Enable TX, RX, RXNE interrupt.

        // NVIC
        NVIC_SetPriority(USART2_IRQn, 5);
        NVIC_EnableIRQ(USART2_IRQn);

        // Buffer
        buffer.reset();
    }

    void close() override {
        uart_instance->CR1 = 0x0;  // Disable UART.
    }

    // Print string.
    void print(const char* out) override {
        while (*out) {
            uart_instance->DR = static_cast<unsigned char>(*out);
            while (!(uart_instance->SR & (USART_SR_TXE)))
                ;  // Wait for byte to be transmitted.
            out++;
        }
    }
    void print(const char* out, std::size_t size) override {
        for (std::size_t i = 0; i < size; ++i) {
            uart_instance->DR = static_cast<unsigned char>(*out);
            while (!(uart_instance->SR & (USART_SR_TXE)))
                ;  // Wait for byte to be transmitted.
        }
    }

    // Get first available character. Return EOF (-1) if no key is pressed.
    int get_char() override {
        static constexpr int eof = -1;

        if (auto key = buffer.pop()) {
            return static_cast<int>(*key);
        }
        return eof;
    }
};

console::Console_io& get_console_io_impl() {
    static IO_vcp io_vcp;
    return io_vcp;
}

extern "C" void USART2_IRQHandler() {
    const std::uint32_t status = uart_instance->SR;

    if ((status & USART_SR_ORE) || (status & USART_SR_NE) || (status & USART_SR_FE) || (status & USART_SR_PE)) {
        // Clear error flag by reading data register.
        const std::uint8_t data = uart_instance->DR;
        (void)data;
    }

    if (status & USART_SR_RXNE) {
        buffer.push(uart_instance->DR);
    }
}
