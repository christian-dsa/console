/**
 * Copyright 2022 Christian Daigneault-St-Arnaud
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions
 *    and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 *    following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <algorithm>
#include <cstdint>
// NOLINTBEGIN(readability-identifier-naming, hicpp-no-assembler, cppcoreguidelines-avoid-c-arrays,
// cppcoreguidelines-pro-type-reinterpret-cast, bugprone-reserved-identifier)

//  Macros
#define WEAK __attribute__((weak))

// C++ library startup entry point
extern "C" {
extern void __libc_init_array(void);
}

// Linker symbols
extern std::uintptr_t _estack;  // Top of stack (RAM)

// .data
extern std::uintptr_t _sidata;  // data start   (FLASH)
extern std::uintptr_t _sdata;   // data start   (RAM)
extern std::uintptr_t _edata;   // data end     (RAM)

// .bss
extern std::uintptr_t _sbss;  // bss start    (RAM)
extern std::uintptr_t _ebss;  // bss end      (RAM)

// Default interrupt handler
void default_handler() {
    while (true) {
        // Interrupt has no handler.
    }
}

extern "C" {

// Reset handle declaration
void Reset_Handler();

// Core exception handlers.
WEAK void NMI_Handler() {
    default_handler();
}
WEAK void HardFault_Handler() {
    default_handler();
}
WEAK void MemManage_Handler() {
    default_handler();
}
WEAK void BusFault_Handler() {
    default_handler();
}
WEAK void UsageFault_Handler() {
    default_handler();
}
WEAK void SVC_Handler() {
    default_handler();
}
WEAK void DebugMon_Handler() {
    default_handler();
}
WEAK void PendSV_Handler() {
    default_handler();
}
WEAK void SysTick_Handler() {
    default_handler();
}

// Peripherals exception handlers.
WEAK void WWDG_IRQHandler() {
    default_handler();
}
WEAK void PVD_IRQHandler() {
    default_handler();
}
WEAK void TAMP_STAMP_IRQHandler() {
    default_handler();
}
WEAK void RTC_WKUP_IRQHandler() {
    default_handler();
}
WEAK void FLASH_IRQHandler() {
    default_handler();
}
WEAK void RCC_IRQHandler() {
    default_handler();
}
WEAK void EXTI0_IRQHandler() {
    default_handler();
}
WEAK void EXTI1_IRQHandler() {
    default_handler();
}
WEAK void EXTI2_IRQHandler() {
    default_handler();
}
WEAK void EXTI3_IRQHandler() {
    default_handler();
}
WEAK void EXTI4_IRQHandler() {
    default_handler();
}
WEAK void DMA1_Stream0_IRQHandler() {
    default_handler();
}
WEAK void DMA1_Stream1_IRQHandler() {
    default_handler();
}
WEAK void DMA1_Stream2_IRQHandler() {
    default_handler();
}
WEAK void DMA1_Stream3_IRQHandler() {
    default_handler();
}
WEAK void DMA1_Stream4_IRQHandler() {
    default_handler();
}
WEAK void DMA1_Stream5_IRQHandler() {
    default_handler();
}
WEAK void DMA1_Stream6_IRQHandler() {
    default_handler();
}
WEAK void ADC_IRQHandler() {
    default_handler();
}
WEAK void CAN1_TX_IRQHandler() {
    default_handler();
}
WEAK void CAN1_RX0_IRQHandler() {
    default_handler();
}
WEAK void CAN1_RX1_IRQHandler() {
    default_handler();
}
WEAK void CAN1_SCE_IRQHandler() {
    default_handler();
}
WEAK void EXTI9_5_IRQHandler() {
    default_handler();
}
WEAK void TIM1_BRK_TIM9_IRQHandler() {
    default_handler();
}
WEAK void TIM1_UP_TIM10_IRQHandler() {
    default_handler();
}
WEAK void TIM1_TRG_COM_TIM11_IRQHandler() {
    default_handler();
}
WEAK void TIM1_CC_IRQHandler() {
    default_handler();
}
WEAK void TIM2_IRQHandler() {
    default_handler();
}
WEAK void TIM3_IRQHandler() {
    default_handler();
}
WEAK void TIM4_IRQHandler() {
    default_handler();
}
WEAK void I2C1_EV_IRQHandler() {
    default_handler();
}
WEAK void I2C1_ER_IRQHandler() {
    default_handler();
}
WEAK void I2C2_EV_IRQHandler() {
    default_handler();
}
WEAK void I2C2_ER_IRQHandler() {
    default_handler();
}
WEAK void SPI1_IRQHandler() {
    default_handler();
}
WEAK void SPI2_IRQHandler() {
    default_handler();
}
WEAK void USART1_IRQHandler() {
    default_handler();
}
WEAK void USART2_IRQHandler() {
    default_handler();
}
WEAK void USART3_IRQHandler() {
    default_handler();
}
WEAK void EXTI15_10_IRQHandler() {
    default_handler();
}
WEAK void RTC_Alarm_IRQHandler() {
    default_handler();
}
WEAK void OTG_FS_WKUP_IRQHandler() {
    default_handler();
}
WEAK void TIM8_BRK_TIM12_IRQHandler() {
    default_handler();
}
WEAK void TIM8_UP_TIM13_IRQHandler() {
    default_handler();
}
WEAK void TIM8_TRG_COM_TIM14_IRQHandler() {
    default_handler();
}
WEAK void TIM8_CC_IRQHandler() {
    default_handler();
}
WEAK void DMA1_Stream7_IRQHandler() {
    default_handler();
}
WEAK void FMC_IRQHandler() {
    default_handler();
}
WEAK void SDIO_IRQHandler() {
    default_handler();
}
WEAK void TIM5_IRQHandler() {
    default_handler();
}
WEAK void SPI3_IRQHandler() {
    default_handler();
}
WEAK void UART4_IRQHandler() {
    default_handler();
}
WEAK void UART5_IRQHandler() {
    default_handler();
}
WEAK void TIM6_DAC_IRQHandler() {
    default_handler();
}
WEAK void TIM7_IRQHandler() {
    default_handler();
}
WEAK void DMA2_Stream0_IRQHandler() {
    default_handler();
}
WEAK void DMA2_Stream1_IRQHandler() {
    default_handler();
}
WEAK void DMA2_Stream2_IRQHandler() {
    default_handler();
}
WEAK void DMA2_Stream3_IRQHandler() {
    default_handler();
}
WEAK void DMA2_Stream4_IRQHandler() {
    default_handler();
}
WEAK void CAN2_TX_IRQHandler() {
    default_handler();
}
WEAK void CAN2_RX0_IRQHandler() {
    default_handler();
}
WEAK void CAN2_RX1_IRQHandler() {
    default_handler();
}
WEAK void CAN2_SCE_IRQHandler() {
    default_handler();
}
WEAK void OTG_FS_IRQHandler() {
    default_handler();
}
WEAK void DMA2_Stream5_IRQHandler() {
    default_handler();
}
WEAK void DMA2_Stream6_IRQHandler() {
    default_handler();
}
WEAK void DMA2_Stream7_IRQHandler() {
    default_handler();
}
WEAK void USART6_IRQHandler() {
    default_handler();
}
WEAK void I2C3_EV_IRQHandler() {
    default_handler();
}
WEAK void I2C3_ER_IRQHandler() {
    default_handler();
}
WEAK void OTG_HS_EP1_OUT_IRQHandler() {
    default_handler();
}
WEAK void OTG_HS_EP1_IN_IRQHandler() {
    default_handler();
}
WEAK void OTG_HS_WKUP_IRQHandler() {
    default_handler();
}
WEAK void OTG_HS_IRQHandler() {
    default_handler();
}
WEAK void DCMI_IRQHandler() {
    default_handler();
}
WEAK void FPU_IRQHandler() {
    default_handler();
}
WEAK void SPI4_IRQHandler() {
    default_handler();
}
WEAK void SAI1_IRQHandler() {
    default_handler();
}
WEAK void SAI2_IRQHandler() {
    default_handler();
}
WEAK void QUADSPI_IRQHandler() {
    default_handler();
}
WEAK void CEC_IRQHandler() {
    default_handler();
}
WEAK void SPDIF_RX_IRQHandler() {
    default_handler();
}
WEAK void FMPI2C1_EV_IRQHandler() {
    default_handler();
}
WEAK void FMPI2C1_ER_IRQHandler() {
    default_handler();
}

// main
extern int main();

// CMSIS SystemInit
extern void SystemInit();

}  // extern "C"

// Vector table
using handler_ptr = void (*)();
extern handler_ptr const g_pfnVectors[];  // declaration to see g_pfnVectors in map file instead of ALIGN(4)

__attribute__((used, section(".isr_vector"))) handler_ptr const g_pfnVectors[] = {
    reinterpret_cast<handler_ptr>(&_estack),
    Reset_Handler,
    NMI_Handler,
    HardFault_Handler,
    MemManage_Handler,
    BusFault_Handler,
    UsageFault_Handler,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    SVC_Handler,
    DebugMon_Handler,
    nullptr,
    PendSV_Handler,
    SysTick_Handler,

    WWDG_IRQHandler,               /* Window WatchDog              */
    PVD_IRQHandler,                /* PVD through EXTI Line detection */
    TAMP_STAMP_IRQHandler,         /* Tamper and T,imeStamps through the EXTI line */
    RTC_WKUP_IRQHandler,           /* RTC Wakeup through the EXTI line */
    FLASH_IRQHandler,              /* FLASH                        */
    RCC_IRQHandler,                /* RCC                          */
    EXTI0_IRQHandler,              /* EXTI Line0                   */
    EXTI1_IRQHandler,              /* EXTI Line1                   */
    EXTI2_IRQHandler,              /* EXTI Line2                   */
    EXTI3_IRQHandler,              /* EXTI Line3                   */
    EXTI4_IRQHandler,              /* EXTI Line4                   */
    DMA1_Stream0_IRQHandler,       /* DMA1 Stream 0                */
    DMA1_Stream1_IRQHandler,       /* DMA1 Stream 1                */
    DMA1_Stream2_IRQHandler,       /* DMA1 Stream 2                */
    DMA1_Stream3_IRQHandler,       /* DMA1 Stream 3                */
    DMA1_Stream4_IRQHandler,       /* DMA1 Stream 4                */
    DMA1_Stream5_IRQHandler,       /* DMA1 Stream 5                */
    DMA1_Stream6_IRQHandler,       /* DMA1 Stream 6                */
    ADC_IRQHandler,                /* ADC1, ADC2 and ADC3s         */
    CAN1_TX_IRQHandler,            /* CAN1 TX                      */
    CAN1_RX0_IRQHandler,           /* CAN1 RX0                     */
    CAN1_RX1_IRQHandler,           /* CAN1 RX1                     */
    CAN1_SCE_IRQHandler,           /* CAN1 SCE                     */
    EXTI9_5_IRQHandler,            /* External Line[9:5]s          */
    TIM1_BRK_TIM9_IRQHandler,      /* TIM1 Break and TIM9          */
    TIM1_UP_TIM10_IRQHandler,      /* TIM1 Update and TIM10        */
    TIM1_TRG_COM_TIM11_IRQHandler, /* TIM1 Trigger and Commutation and TIM11 */
    TIM1_CC_IRQHandler,            /* TIM1 Capture Compare         */
    TIM2_IRQHandler,               /* TIM2                         */
    TIM3_IRQHandler,               /* TIM3                         */
    TIM4_IRQHandler,               /* TIM4                         */
    I2C1_EV_IRQHandler,            /* I2C1 Event                   */
    I2C1_ER_IRQHandler,            /* I2C1 Error                   */
    I2C2_EV_IRQHandler,            /* I2C2 Event                   */
    I2C2_ER_IRQHandler,            /* I2C2 Error                   */
    SPI1_IRQHandler,               /* SPI1                         */
    SPI2_IRQHandler,               /* SPI2                         */
    USART1_IRQHandler,             /* USART1                       */
    USART2_IRQHandler,             /* USART2                       */
    USART3_IRQHandler,             /* USART3                       */
    EXTI15_10_IRQHandler,          /* External Line[15:10]s        */
    RTC_Alarm_IRQHandler,          /* RTC Alarm (A and B) through EXTI Line */
    OTG_FS_WKUP_IRQHandler,        /* USB OTG FS Wakeup through EXTI line */
    TIM8_BRK_TIM12_IRQHandler,     /* TIM8 Break and TIM12         */
    TIM8_UP_TIM13_IRQHandler,      /* TIM8 Update and TIM13        */
    TIM8_TRG_COM_TIM14_IRQHandler, /* TIM8 Trigger and Commutation and TIM14 */
    TIM8_CC_IRQHandler,            /* TIM8 Capture Compare         */
    DMA1_Stream7_IRQHandler,       /* DMA1 Stream7                 */
    FMC_IRQHandler,                /* FMC                          */
    SDIO_IRQHandler,               /* SDIO                         */
    TIM5_IRQHandler,               /* TIM5                         */
    SPI3_IRQHandler,               /* SPI3                         */
    UART4_IRQHandler,              /* UART4                        */
    UART5_IRQHandler,              /* UART5                        */
    TIM6_DAC_IRQHandler,           /* TIM6 and DAC1&2 underrun errors */
    TIM7_IRQHandler,               /* TIM7                         */
    DMA2_Stream0_IRQHandler,       /* DMA2 Stream 0                */
    DMA2_Stream1_IRQHandler,       /* DMA2 Stream 1                */
    DMA2_Stream2_IRQHandler,       /* DMA2 Stream 2                */
    DMA2_Stream3_IRQHandler,       /* DMA2 Stream 3                */
    DMA2_Stream4_IRQHandler,       /* DMA2 Stream 4                */
    nullptr,                       /* Reserved                     */
    nullptr,                       /* Reserved                     */
    CAN2_TX_IRQHandler,            /* CAN2 TX                      */
    CAN2_RX0_IRQHandler,           /* CAN2 RX0                     */
    CAN2_RX1_IRQHandler,           /* CAN2 RX1                     */
    CAN2_SCE_IRQHandler,           /* CAN2 SCE                     */
    OTG_FS_IRQHandler,             /* USB OTG FS                   */
    DMA2_Stream5_IRQHandler,       /* DMA2 Stream 5                */
    DMA2_Stream6_IRQHandler,       /* DMA2 Stream 6                */
    DMA2_Stream7_IRQHandler,       /* DMA2 Stream 7                */
    USART6_IRQHandler,             /* USART6                       */
    I2C3_EV_IRQHandler,            /* I2C3 event                   */
    I2C3_ER_IRQHandler,            /* I2C3 error                   */
    OTG_HS_EP1_OUT_IRQHandler,     /* USB OTG HS End Point 1 Out   */
    OTG_HS_EP1_IN_IRQHandler,      /* USB OTG HS End Point 1 In    */
    OTG_HS_WKUP_IRQHandler,        /* USB OTG HS Wakeup through EXTI */
    OTG_HS_IRQHandler,             /* USB OTG HS                   */
    DCMI_IRQHandler,               /* DCMI                         */
    nullptr,                       /* Reserved                     */
    nullptr,                       /* Reserved                     */
    FPU_IRQHandler,                /* FPU                          */
    nullptr,                       /* Reserved                     */
    nullptr,                       /* Reserved                     */
    SPI4_IRQHandler,               /* SPI4                         */
    nullptr,                       /* Reserved                     */
    nullptr,                       /* Reserved                     */
    SAI1_IRQHandler,               /* SAI1                         */
    nullptr,                       /* Reserved                     */
    nullptr,                       /* Reserved                     */
    nullptr,                       /* Reserved                     */
    SAI2_IRQHandler,               /* SAI2                         */
    QUADSPI_IRQHandler,            /* QuadSPI                      */
    CEC_IRQHandler,                /* CEC                          */
    SPDIF_RX_IRQHandler,           /* SPDIF RX                     */
    FMPI2C1_EV_IRQHandler,         /* FMPI2C 1 Event               */
    FMPI2C1_ER_IRQHandler,         /* FMPI2C 1 Error               */
};

__attribute__((used, section(".text"))) void Reset_Handler() {
    __asm volatile("cpsid i");  // Disable interrupts

    std::fill(&_sbss, &_ebss, 0U);  // Clear .bss section

    const std::size_t data_size = (&_edata - &_sdata);
    std::copy(&_sidata, &_sidata + data_size, &_sdata);  // Initialize .data section

    __libc_init_array();  // C++ library initialisation

    SystemInit();

    __asm volatile("cpsie i");  // Enable interrupts

    __asm volatile("bl main");  // Jump to main

    while (true) {
        // Returned from main. Infinite loop for debug purpose.
    }
}
// NOLINTEND(readability-identifier-naming, hicpp-no-assembler, cppcoreguidelines-avoid-c-arrays,
// cppcoreguidelines-pro-type-reinterpret-cast)
