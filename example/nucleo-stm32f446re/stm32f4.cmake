# GCC compiler and linker flags for Cortex-M4
set(CMAKE_C_FLAGS "-mcpu=cortex-m4 -march=armv7e-m -mthumb -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-move-loop-invariants -fno-stack-protector -Wall -Wextra -Wundef -ffreestanding -fno-common -fno-omit-frame-pointer -fsingle-precision-constant -Wdouble-promotion -Wno-unused-function"
    CACHE STRING
    "Flags used by the C compiler during all build types."
    FORCE
    )

set(CMAKE_CXX_FLAGS "-mcpu=cortex-m4 -march=armv7e-m -mthumb -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-move-loop-invariants -fno-stack-protector -Wall -Wextra -Wundef -ffreestanding -fno-exceptions -fno-rtti -fno-use-cxa-atexit -fno-threadsafe-statics -fno-common -fno-omit-frame-pointer -fsingle-precision-constant -Wdouble-promotion -Wno-unused-function"
    CACHE STRING
    "Flags used by the C++ compiler during all build types."
    FORCE
    )

set(CMAKE_ASM_FLAGS "-mcpu=cortex-m4 -march=armv7e-m -mthumb -x assembler-with-cpp"
    CACHE STRING
    "Flags used by the ASM compiler during all build types."
    FORCE
    )

set(REL_SRC_PATH ".")
set(CMAKE_C_FLAGS_DEBUG "-g3 -fdebug-prefix-map=${CMAKE_SOURCE_DIR}=${REL_SRC_PATH}"
    CACHE STRING
    "Flags used by the C compiler during debug build types."
    FORCE
    )
set(CMAKE_CXX_FLAGS_DEBUG "-g3  -fdebug-prefix-map=${CMAKE_SOURCE_DIR}=${REL_SRC_PATH}"
    CACHE STRING
    "Flags used by the C++ compiler during debug build types."
    FORCE
    )

set(CMAKE_ASM_FLAGS_DEBUG "-g3 -fdebug-prefix-map=${CMAKE_SOURCE_DIR}=${REL_SRC_PATH}"
    CACHE STRING
    "Flags used by the ASM compiler during debug build types."
    FORCE
    )

# Disable linker warning for rwx segment section in GCC >= 12.
string(SUBSTRING ${CMAKE_CXX_COMPILER_VERSION} 0 2 CXX_COMPILER_MAJOR_VERSION)
set(LINKER_WARNING_RWX_SEGMENT "")
if(${CXX_COMPILER_MAJOR_VERSION} GREATER_EQUAL 12)
    set(LINKER_WARNING_RWX_SEGMENT "-Wl,--no-warn-rwx-segments")
endif()

set(CMAKE_EXE_LINKER_FLAGS
    "-mcpu=cortex-m4 -march=armv7e-m -mthumb -specs=nosys.specs -specs=nano.specs -Wl,--gc-sections -Wl,--print-memory-usage -fno-exceptions -fno-rtti -lm ${LINKER_WARNING_RWX_SEGMENT}"
    CACHE STRING
    "Flags used by the linker during all build types."
    FORCE
    )
