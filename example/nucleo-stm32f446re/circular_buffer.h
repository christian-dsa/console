/**
 * Copyright 2022 Christian Daigneault-St-Arnaud
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions
 *    and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 *    following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef CIRCULAR_BUFFER_H_
#define CIRCULAR_BUFFER_H_

#include <array>
#include <cstdint>
#include <optional>

template<typename data_type, std::size_t size>
class circular_buffer {
public:
    circular_buffer()
    : buffer_start(buffer.begin()),
      buffer_end(buffer.end()),
      read_it(buffer.begin()),
      write_it(buffer.begin()),
      free_space(size){};

    std::size_t capacity() {
        return buffer.max_size();
    }

    std::size_t available() {
        return buffer.max_size() - free_space;
    };

    bool empty() {
        if (free_space == buffer.max_size()) {
            return true;
        } else {
            return false;
        }
    }

    bool full() {
        if (free_space == 0) {
            return true;
        } else {
            return false;
        }
    }

    void push(const data_type& data) {
        if (write_it == buffer_end) {
            write_it = buffer_start;
        }

        *write_it = data;
        ++write_it;

        if (!free_space) {
            read_it = write_it;
        } else {
            --free_space;
        }
    }

    std::optional<data_type> pop() {
        if (empty()) {
            return std::nullopt;
        }

        if (read_it == buffer_end) {
            read_it = buffer_start;
        }

        data_type out = *read_it;
        ++read_it;
        ++free_space;

        return out;
    }

    void reset() {
        read_it    = buffer_start;
        write_it   = read_it;
        free_space = buffer.max_size();
    }

private:
    typename std::array<data_type, size>           buffer{};
    typename std::array<data_type, size>::iterator buffer_start;
    typename std::array<data_type, size>::iterator buffer_end;
    typename std::array<data_type, size>::iterator read_it;
    typename std::array<data_type, size>::iterator write_it;
    std::size_t                                    free_space;
};

#endif /* CIRCULAR_BUFFER_H_ */
